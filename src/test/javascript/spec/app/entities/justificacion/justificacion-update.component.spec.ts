import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { SicuTestModule } from '../../../test.module';
import { JustificacionUpdateComponent } from 'app/entities/justificacion/justificacion-update.component';
import { JustificacionService } from 'app/entities/justificacion/justificacion.service';
import { Justificacion } from 'app/shared/model/justificacion.model';

describe('Component Tests', () => {
  describe('Justificacion Management Update Component', () => {
    let comp: JustificacionUpdateComponent;
    let fixture: ComponentFixture<JustificacionUpdateComponent>;
    let service: JustificacionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [JustificacionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(JustificacionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JustificacionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JustificacionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Justificacion(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Justificacion();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
