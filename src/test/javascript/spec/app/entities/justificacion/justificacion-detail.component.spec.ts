import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SicuTestModule } from '../../../test.module';
import { JustificacionDetailComponent } from 'app/entities/justificacion/justificacion-detail.component';
import { Justificacion } from 'app/shared/model/justificacion.model';

describe('Component Tests', () => {
  describe('Justificacion Management Detail Component', () => {
    let comp: JustificacionDetailComponent;
    let fixture: ComponentFixture<JustificacionDetailComponent>;
    const route = ({ data: of({ justificacion: new Justificacion(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [JustificacionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(JustificacionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(JustificacionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.justificacion).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
