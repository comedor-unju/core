import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { JustificacionService } from 'app/entities/justificacion/justificacion.service';
import { IJustificacion, Justificacion } from 'app/shared/model/justificacion.model';
import { MotivoInasistencia } from 'app/shared/model/enumerations/motivo-inasistencia.model';

describe('Service Tests', () => {
  describe('Justificacion Service', () => {
    let injector: TestBed;
    let service: JustificacionService;
    let httpMock: HttpTestingController;
    let elemDefault: IJustificacion;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(JustificacionService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Justificacion(0, currentDate, currentDate, MotivoInasistencia.ENFERMEDAD, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            desde: currentDate.format(DATE_FORMAT),
            hasta: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Justificacion', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            desde: currentDate.format(DATE_FORMAT),
            hasta: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            desde: currentDate,
            hasta: currentDate
          },
          returnedFromService
        );
        service
          .create(new Justificacion(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Justificacion', () => {
        const returnedFromService = Object.assign(
          {
            desde: currentDate.format(DATE_FORMAT),
            hasta: currentDate.format(DATE_FORMAT),
            motivo: 'BBBBBB',
            observaciones: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            desde: currentDate,
            hasta: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Justificacion', () => {
        const returnedFromService = Object.assign(
          {
            desde: currentDate.format(DATE_FORMAT),
            hasta: currentDate.format(DATE_FORMAT),
            motivo: 'BBBBBB',
            observaciones: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            desde: currentDate,
            hasta: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Justificacion', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
