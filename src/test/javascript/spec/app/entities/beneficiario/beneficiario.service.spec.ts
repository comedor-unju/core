import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import { BeneficiarioService } from 'app/entities/beneficiario/beneficiario.service';
import { IBeneficiario, Beneficiario } from 'app/shared/model/beneficiario.model';
import { UnidadAcademica } from 'app/shared/model/enumerations/unidad-academica.model';

describe('Service Tests', () => {
  describe('Beneficiario Service', () => {
    let injector: TestBed;
    let service: BeneficiarioService;
    let httpMock: HttpTestingController;
    let elemDefault: IBeneficiario;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(BeneficiarioService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Beneficiario(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', UnidadAcademica.FI, 'image/png', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Beneficiario', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new Beneficiario(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Beneficiario', () => {
        const returnedFromService = Object.assign(
          {
            apellido: 'BBBBBB',
            nombres: 'BBBBBB',
            dni: 'BBBBBB',
            idBiblioteca: 'BBBBBB',
            unidadAcademica: 'BBBBBB',
            foto: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Beneficiario', () => {
        const returnedFromService = Object.assign(
          {
            apellido: 'BBBBBB',
            nombres: 'BBBBBB',
            dni: 'BBBBBB',
            idBiblioteca: 'BBBBBB',
            unidadAcademica: 'BBBBBB',
            foto: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Beneficiario', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
