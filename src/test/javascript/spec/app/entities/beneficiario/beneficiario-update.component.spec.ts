import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { SicuTestModule } from '../../../test.module';
import { BeneficiarioUpdateComponent } from 'app/entities/beneficiario/beneficiario-update.component';
import { BeneficiarioService } from 'app/entities/beneficiario/beneficiario.service';
import { Beneficiario } from 'app/shared/model/beneficiario.model';

describe('Component Tests', () => {
  describe('Beneficiario Management Update Component', () => {
    let comp: BeneficiarioUpdateComponent;
    let fixture: ComponentFixture<BeneficiarioUpdateComponent>;
    let service: BeneficiarioService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [BeneficiarioUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BeneficiarioUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BeneficiarioUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BeneficiarioService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Beneficiario(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Beneficiario();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
