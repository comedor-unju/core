import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SicuTestModule } from '../../../test.module';
import { BeneficiarioDeleteDialogComponent } from 'app/entities/beneficiario/beneficiario-delete-dialog.component';
import { BeneficiarioService } from 'app/entities/beneficiario/beneficiario.service';

describe('Component Tests', () => {
  describe('Beneficiario Management Delete Component', () => {
    let comp: BeneficiarioDeleteDialogComponent;
    let fixture: ComponentFixture<BeneficiarioDeleteDialogComponent>;
    let service: BeneficiarioService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [BeneficiarioDeleteDialogComponent]
      })
        .overrideTemplate(BeneficiarioDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BeneficiarioDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BeneficiarioService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
