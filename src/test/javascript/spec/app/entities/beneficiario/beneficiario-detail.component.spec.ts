import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SicuTestModule } from '../../../test.module';
import { BeneficiarioDetailComponent } from 'app/entities/beneficiario/beneficiario-detail.component';
import { Beneficiario } from 'app/shared/model/beneficiario.model';

describe('Component Tests', () => {
  describe('Beneficiario Management Detail Component', () => {
    let comp: BeneficiarioDetailComponent;
    let fixture: ComponentFixture<BeneficiarioDetailComponent>;
    const route = ({ data: of({ beneficiario: new Beneficiario(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [BeneficiarioDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BeneficiarioDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BeneficiarioDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.beneficiario).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
