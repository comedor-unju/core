import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SicuTestModule } from '../../../test.module';
import { BecaDetailComponent } from 'app/entities/beca/beca-detail.component';
import { Beca } from 'app/shared/model/beca.model';

describe('Component Tests', () => {
  describe('Beca Management Detail Component', () => {
    let comp: BecaDetailComponent;
    let fixture: ComponentFixture<BecaDetailComponent>;
    const route = ({ data: of({ beca: new Beca(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [BecaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BecaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BecaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.beca).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
