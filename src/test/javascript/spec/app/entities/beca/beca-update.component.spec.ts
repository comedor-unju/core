import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { SicuTestModule } from '../../../test.module';
import { BecaUpdateComponent } from 'app/entities/beca/beca-update.component';
import { BecaService } from 'app/entities/beca/beca.service';
import { Beca } from 'app/shared/model/beca.model';

describe('Component Tests', () => {
  describe('Beca Management Update Component', () => {
    let comp: BecaUpdateComponent;
    let fixture: ComponentFixture<BecaUpdateComponent>;
    let service: BecaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [BecaUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BecaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BecaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BecaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Beca(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Beca();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
