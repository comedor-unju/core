import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SicuTestModule } from '../../../test.module';
import { BecaDeleteDialogComponent } from 'app/entities/beca/beca-delete-dialog.component';
import { BecaService } from 'app/entities/beca/beca.service';

describe('Component Tests', () => {
  describe('Beca Management Delete Component', () => {
    let comp: BecaDeleteDialogComponent;
    let fixture: ComponentFixture<BecaDeleteDialogComponent>;
    let service: BecaService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [BecaDeleteDialogComponent]
      })
        .overrideTemplate(BecaDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BecaDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BecaService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
