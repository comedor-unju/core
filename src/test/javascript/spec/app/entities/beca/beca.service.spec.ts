import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { BecaService } from 'app/entities/beca/beca.service';
import { IBeca, Beca } from 'app/shared/model/beca.model';

describe('Service Tests', () => {
  describe('Beca Service', () => {
    let injector: TestBed;
    let service: BecaService;
    let httpMock: HttpTestingController;
    let elemDefault: IBeca;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(BecaService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Beca(0, currentDate, currentDate, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            fechaDesde: currentDate.format(DATE_FORMAT),
            fechaHasta: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Beca', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            fechaDesde: currentDate.format(DATE_FORMAT),
            fechaHasta: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            fechaDesde: currentDate,
            fechaHasta: currentDate
          },
          returnedFromService
        );
        service
          .create(new Beca(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Beca', () => {
        const returnedFromService = Object.assign(
          {
            fechaDesde: currentDate.format(DATE_FORMAT),
            fechaHasta: currentDate.format(DATE_FORMAT),
            habilitacion: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            fechaDesde: currentDate,
            fechaHasta: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Beca', () => {
        const returnedFromService = Object.assign(
          {
            fechaDesde: currentDate.format(DATE_FORMAT),
            fechaHasta: currentDate.format(DATE_FORMAT),
            habilitacion: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            fechaDesde: currentDate,
            fechaHasta: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Beca', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
