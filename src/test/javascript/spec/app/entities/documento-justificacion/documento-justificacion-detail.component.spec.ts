import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SicuTestModule } from '../../../test.module';
import { DocumentoJustificacionDetailComponent } from 'app/entities/documento-justificacion/documento-justificacion-detail.component';
import { DocumentoJustificacion } from 'app/shared/model/documento-justificacion.model';

describe('Component Tests', () => {
  describe('DocumentoJustificacion Management Detail Component', () => {
    let comp: DocumentoJustificacionDetailComponent;
    let fixture: ComponentFixture<DocumentoJustificacionDetailComponent>;
    const route = ({ data: of({ documentoJustificacion: new DocumentoJustificacion(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [DocumentoJustificacionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DocumentoJustificacionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DocumentoJustificacionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.documentoJustificacion).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
