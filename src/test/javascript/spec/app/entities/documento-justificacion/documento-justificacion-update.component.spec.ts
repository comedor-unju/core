import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { SicuTestModule } from '../../../test.module';
import { DocumentoJustificacionUpdateComponent } from 'app/entities/documento-justificacion/documento-justificacion-update.component';
import { DocumentoJustificacionService } from 'app/entities/documento-justificacion/documento-justificacion.service';
import { DocumentoJustificacion } from 'app/shared/model/documento-justificacion.model';

describe('Component Tests', () => {
  describe('DocumentoJustificacion Management Update Component', () => {
    let comp: DocumentoJustificacionUpdateComponent;
    let fixture: ComponentFixture<DocumentoJustificacionUpdateComponent>;
    let service: DocumentoJustificacionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SicuTestModule],
        declarations: [DocumentoJustificacionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DocumentoJustificacionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DocumentoJustificacionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DocumentoJustificacionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DocumentoJustificacion(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DocumentoJustificacion();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
