package ar.edu.unju.sicu.web.rest;

import ar.edu.unju.sicu.SicuApp;
import ar.edu.unju.sicu.domain.DocumentoJustificacion;
import ar.edu.unju.sicu.domain.Justificacion;
import ar.edu.unju.sicu.repository.DocumentoJustificacionRepository;
import ar.edu.unju.sicu.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ar.edu.unju.sicu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DocumentoJustificacionResource} REST controller.
 */
@SpringBootTest(classes = SicuApp.class)
public class DocumentoJustificacionResourceIT {

    private static final String DEFAULT_NOMBRE_ARCHIVO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_ARCHIVO = "BBBBBBBBBB";

    private static final byte[] DEFAULT_FILE_CONTENT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE_CONTENT = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FILE_CONTENT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_CONTENT_TYPE = "image/png";

    @Autowired
    private DocumentoJustificacionRepository documentoJustificacionRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDocumentoJustificacionMockMvc;

    private DocumentoJustificacion documentoJustificacion;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DocumentoJustificacionResource documentoJustificacionResource = new DocumentoJustificacionResource(documentoJustificacionRepository);
        this.restDocumentoJustificacionMockMvc = MockMvcBuilders.standaloneSetup(documentoJustificacionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DocumentoJustificacion createEntity(EntityManager em) {
        DocumentoJustificacion documentoJustificacion = new DocumentoJustificacion()
            .nombreArchivo(DEFAULT_NOMBRE_ARCHIVO)
            .fileContent(DEFAULT_FILE_CONTENT)
            .fileContentContentType(DEFAULT_FILE_CONTENT_CONTENT_TYPE);
        // Add required entity
        Justificacion justificacion;
        if (TestUtil.findAll(em, Justificacion.class).isEmpty()) {
            justificacion = JustificacionResourceIT.createEntity(em);
            em.persist(justificacion);
            em.flush();
        } else {
            justificacion = TestUtil.findAll(em, Justificacion.class).get(0);
        }
        documentoJustificacion.setJustificacion(justificacion);
        return documentoJustificacion;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DocumentoJustificacion createUpdatedEntity(EntityManager em) {
        DocumentoJustificacion documentoJustificacion = new DocumentoJustificacion()
            .nombreArchivo(UPDATED_NOMBRE_ARCHIVO)
            .fileContent(UPDATED_FILE_CONTENT)
            .fileContentContentType(UPDATED_FILE_CONTENT_CONTENT_TYPE);
        // Add required entity
        Justificacion justificacion;
        if (TestUtil.findAll(em, Justificacion.class).isEmpty()) {
            justificacion = JustificacionResourceIT.createUpdatedEntity(em);
            em.persist(justificacion);
            em.flush();
        } else {
            justificacion = TestUtil.findAll(em, Justificacion.class).get(0);
        }
        documentoJustificacion.setJustificacion(justificacion);
        return documentoJustificacion;
    }

    @BeforeEach
    public void initTest() {
        documentoJustificacion = createEntity(em);
    }

    @Test
    @Transactional
    public void createDocumentoJustificacion() throws Exception {
        int databaseSizeBeforeCreate = documentoJustificacionRepository.findAll().size();

        // Create the DocumentoJustificacion
        restDocumentoJustificacionMockMvc.perform(post("/api/documento-justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoJustificacion)))
            .andExpect(status().isCreated());

        // Validate the DocumentoJustificacion in the database
        List<DocumentoJustificacion> documentoJustificacionList = documentoJustificacionRepository.findAll();
        assertThat(documentoJustificacionList).hasSize(databaseSizeBeforeCreate + 1);
        DocumentoJustificacion testDocumentoJustificacion = documentoJustificacionList.get(documentoJustificacionList.size() - 1);
        assertThat(testDocumentoJustificacion.getNombreArchivo()).isEqualTo(DEFAULT_NOMBRE_ARCHIVO);
        assertThat(testDocumentoJustificacion.getFileContent()).isEqualTo(DEFAULT_FILE_CONTENT);
        assertThat(testDocumentoJustificacion.getFileContentContentType()).isEqualTo(DEFAULT_FILE_CONTENT_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createDocumentoJustificacionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = documentoJustificacionRepository.findAll().size();

        // Create the DocumentoJustificacion with an existing ID
        documentoJustificacion.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDocumentoJustificacionMockMvc.perform(post("/api/documento-justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoJustificacion)))
            .andExpect(status().isBadRequest());

        // Validate the DocumentoJustificacion in the database
        List<DocumentoJustificacion> documentoJustificacionList = documentoJustificacionRepository.findAll();
        assertThat(documentoJustificacionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNombreArchivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentoJustificacionRepository.findAll().size();
        // set the field null
        documentoJustificacion.setNombreArchivo(null);

        // Create the DocumentoJustificacion, which fails.

        restDocumentoJustificacionMockMvc.perform(post("/api/documento-justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoJustificacion)))
            .andExpect(status().isBadRequest());

        List<DocumentoJustificacion> documentoJustificacionList = documentoJustificacionRepository.findAll();
        assertThat(documentoJustificacionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDocumentoJustificacions() throws Exception {
        // Initialize the database
        documentoJustificacionRepository.saveAndFlush(documentoJustificacion);

        // Get all the documentoJustificacionList
        restDocumentoJustificacionMockMvc.perform(get("/api/documento-justificacions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(documentoJustificacion.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreArchivo").value(hasItem(DEFAULT_NOMBRE_ARCHIVO)))
            .andExpect(jsonPath("$.[*].fileContentContentType").value(hasItem(DEFAULT_FILE_CONTENT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].fileContent").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE_CONTENT))));
    }
    
    @Test
    @Transactional
    public void getDocumentoJustificacion() throws Exception {
        // Initialize the database
        documentoJustificacionRepository.saveAndFlush(documentoJustificacion);

        // Get the documentoJustificacion
        restDocumentoJustificacionMockMvc.perform(get("/api/documento-justificacions/{id}", documentoJustificacion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(documentoJustificacion.getId().intValue()))
            .andExpect(jsonPath("$.nombreArchivo").value(DEFAULT_NOMBRE_ARCHIVO))
            .andExpect(jsonPath("$.fileContentContentType").value(DEFAULT_FILE_CONTENT_CONTENT_TYPE))
            .andExpect(jsonPath("$.fileContent").value(Base64Utils.encodeToString(DEFAULT_FILE_CONTENT)));
    }

    @Test
    @Transactional
    public void getNonExistingDocumentoJustificacion() throws Exception {
        // Get the documentoJustificacion
        restDocumentoJustificacionMockMvc.perform(get("/api/documento-justificacions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDocumentoJustificacion() throws Exception {
        // Initialize the database
        documentoJustificacionRepository.saveAndFlush(documentoJustificacion);

        int databaseSizeBeforeUpdate = documentoJustificacionRepository.findAll().size();

        // Update the documentoJustificacion
        DocumentoJustificacion updatedDocumentoJustificacion = documentoJustificacionRepository.findById(documentoJustificacion.getId()).get();
        // Disconnect from session so that the updates on updatedDocumentoJustificacion are not directly saved in db
        em.detach(updatedDocumentoJustificacion);
        updatedDocumentoJustificacion
            .nombreArchivo(UPDATED_NOMBRE_ARCHIVO)
            .fileContent(UPDATED_FILE_CONTENT)
            .fileContentContentType(UPDATED_FILE_CONTENT_CONTENT_TYPE);

        restDocumentoJustificacionMockMvc.perform(put("/api/documento-justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDocumentoJustificacion)))
            .andExpect(status().isOk());

        // Validate the DocumentoJustificacion in the database
        List<DocumentoJustificacion> documentoJustificacionList = documentoJustificacionRepository.findAll();
        assertThat(documentoJustificacionList).hasSize(databaseSizeBeforeUpdate);
        DocumentoJustificacion testDocumentoJustificacion = documentoJustificacionList.get(documentoJustificacionList.size() - 1);
        assertThat(testDocumentoJustificacion.getNombreArchivo()).isEqualTo(UPDATED_NOMBRE_ARCHIVO);
        assertThat(testDocumentoJustificacion.getFileContent()).isEqualTo(UPDATED_FILE_CONTENT);
        assertThat(testDocumentoJustificacion.getFileContentContentType()).isEqualTo(UPDATED_FILE_CONTENT_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingDocumentoJustificacion() throws Exception {
        int databaseSizeBeforeUpdate = documentoJustificacionRepository.findAll().size();

        // Create the DocumentoJustificacion

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDocumentoJustificacionMockMvc.perform(put("/api/documento-justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentoJustificacion)))
            .andExpect(status().isBadRequest());

        // Validate the DocumentoJustificacion in the database
        List<DocumentoJustificacion> documentoJustificacionList = documentoJustificacionRepository.findAll();
        assertThat(documentoJustificacionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDocumentoJustificacion() throws Exception {
        // Initialize the database
        documentoJustificacionRepository.saveAndFlush(documentoJustificacion);

        int databaseSizeBeforeDelete = documentoJustificacionRepository.findAll().size();

        // Delete the documentoJustificacion
        restDocumentoJustificacionMockMvc.perform(delete("/api/documento-justificacions/{id}", documentoJustificacion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DocumentoJustificacion> documentoJustificacionList = documentoJustificacionRepository.findAll();
        assertThat(documentoJustificacionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DocumentoJustificacion.class);
        DocumentoJustificacion documentoJustificacion1 = new DocumentoJustificacion();
        documentoJustificacion1.setId(1L);
        DocumentoJustificacion documentoJustificacion2 = new DocumentoJustificacion();
        documentoJustificacion2.setId(documentoJustificacion1.getId());
        assertThat(documentoJustificacion1).isEqualTo(documentoJustificacion2);
        documentoJustificacion2.setId(2L);
        assertThat(documentoJustificacion1).isNotEqualTo(documentoJustificacion2);
        documentoJustificacion1.setId(null);
        assertThat(documentoJustificacion1).isNotEqualTo(documentoJustificacion2);
    }
}
