package ar.edu.unju.sicu.web.rest;

import ar.edu.unju.sicu.SicuApp;
import ar.edu.unju.sicu.domain.Justificacion;
import ar.edu.unju.sicu.domain.Beneficiario;
import ar.edu.unju.sicu.repository.JustificacionRepository;
import ar.edu.unju.sicu.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ar.edu.unju.sicu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ar.edu.unju.sicu.domain.enumeration.MotivoInasistencia;
/**
 * Integration tests for the {@link JustificacionResource} REST controller.
 */
@SpringBootTest(classes = SicuApp.class)
public class JustificacionResourceIT {

    private static final LocalDate DEFAULT_DESDE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DESDE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_HASTA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_HASTA = LocalDate.now(ZoneId.systemDefault());

    private static final MotivoInasistencia DEFAULT_MOTIVO = MotivoInasistencia.ENFERMEDAD;
    private static final MotivoInasistencia UPDATED_MOTIVO = MotivoInasistencia.SUSPENSION_CLASES;

    private static final String DEFAULT_OBSERVACIONES = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVACIONES = "BBBBBBBBBB";

    @Autowired
    private JustificacionRepository justificacionRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restJustificacionMockMvc;

    private Justificacion justificacion;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JustificacionResource justificacionResource = new JustificacionResource(justificacionRepository);
        this.restJustificacionMockMvc = MockMvcBuilders.standaloneSetup(justificacionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Justificacion createEntity(EntityManager em) {
        Justificacion justificacion = new Justificacion()
            .desde(DEFAULT_DESDE)
            .hasta(DEFAULT_HASTA)
            .motivo(DEFAULT_MOTIVO)
            .observaciones(DEFAULT_OBSERVACIONES);
        // Add required entity
        Beneficiario beneficiario;
        if (TestUtil.findAll(em, Beneficiario.class).isEmpty()) {
            beneficiario = BeneficiarioResourceIT.createEntity(em);
            em.persist(beneficiario);
            em.flush();
        } else {
            beneficiario = TestUtil.findAll(em, Beneficiario.class).get(0);
        }
        justificacion.setBeneficiario(beneficiario);
        return justificacion;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Justificacion createUpdatedEntity(EntityManager em) {
        Justificacion justificacion = new Justificacion()
            .desde(UPDATED_DESDE)
            .hasta(UPDATED_HASTA)
            .motivo(UPDATED_MOTIVO)
            .observaciones(UPDATED_OBSERVACIONES);
        // Add required entity
        Beneficiario beneficiario;
        if (TestUtil.findAll(em, Beneficiario.class).isEmpty()) {
            beneficiario = BeneficiarioResourceIT.createUpdatedEntity(em);
            em.persist(beneficiario);
            em.flush();
        } else {
            beneficiario = TestUtil.findAll(em, Beneficiario.class).get(0);
        }
        justificacion.setBeneficiario(beneficiario);
        return justificacion;
    }

    @BeforeEach
    public void initTest() {
        justificacion = createEntity(em);
    }

    @Test
    @Transactional
    public void createJustificacion() throws Exception {
        int databaseSizeBeforeCreate = justificacionRepository.findAll().size();

        // Create the Justificacion
        restJustificacionMockMvc.perform(post("/api/justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(justificacion)))
            .andExpect(status().isCreated());

        // Validate the Justificacion in the database
        List<Justificacion> justificacionList = justificacionRepository.findAll();
        assertThat(justificacionList).hasSize(databaseSizeBeforeCreate + 1);
        Justificacion testJustificacion = justificacionList.get(justificacionList.size() - 1);
        assertThat(testJustificacion.getDesde()).isEqualTo(DEFAULT_DESDE);
        assertThat(testJustificacion.getHasta()).isEqualTo(DEFAULT_HASTA);
        assertThat(testJustificacion.getMotivo()).isEqualTo(DEFAULT_MOTIVO);
        assertThat(testJustificacion.getObservaciones()).isEqualTo(DEFAULT_OBSERVACIONES);
    }

    @Test
    @Transactional
    public void createJustificacionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = justificacionRepository.findAll().size();

        // Create the Justificacion with an existing ID
        justificacion.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJustificacionMockMvc.perform(post("/api/justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(justificacion)))
            .andExpect(status().isBadRequest());

        // Validate the Justificacion in the database
        List<Justificacion> justificacionList = justificacionRepository.findAll();
        assertThat(justificacionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDesdeIsRequired() throws Exception {
        int databaseSizeBeforeTest = justificacionRepository.findAll().size();
        // set the field null
        justificacion.setDesde(null);

        // Create the Justificacion, which fails.

        restJustificacionMockMvc.perform(post("/api/justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(justificacion)))
            .andExpect(status().isBadRequest());

        List<Justificacion> justificacionList = justificacionRepository.findAll();
        assertThat(justificacionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHastaIsRequired() throws Exception {
        int databaseSizeBeforeTest = justificacionRepository.findAll().size();
        // set the field null
        justificacion.setHasta(null);

        // Create the Justificacion, which fails.

        restJustificacionMockMvc.perform(post("/api/justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(justificacion)))
            .andExpect(status().isBadRequest());

        List<Justificacion> justificacionList = justificacionRepository.findAll();
        assertThat(justificacionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMotivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = justificacionRepository.findAll().size();
        // set the field null
        justificacion.setMotivo(null);

        // Create the Justificacion, which fails.

        restJustificacionMockMvc.perform(post("/api/justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(justificacion)))
            .andExpect(status().isBadRequest());

        List<Justificacion> justificacionList = justificacionRepository.findAll();
        assertThat(justificacionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJustificacions() throws Exception {
        // Initialize the database
        justificacionRepository.saveAndFlush(justificacion);

        // Get all the justificacionList
        restJustificacionMockMvc.perform(get("/api/justificacions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(justificacion.getId().intValue())))
            .andExpect(jsonPath("$.[*].desde").value(hasItem(DEFAULT_DESDE.toString())))
            .andExpect(jsonPath("$.[*].hasta").value(hasItem(DEFAULT_HASTA.toString())))
            .andExpect(jsonPath("$.[*].motivo").value(hasItem(DEFAULT_MOTIVO.toString())))
            .andExpect(jsonPath("$.[*].observaciones").value(hasItem(DEFAULT_OBSERVACIONES)));
    }
    
    @Test
    @Transactional
    public void getJustificacion() throws Exception {
        // Initialize the database
        justificacionRepository.saveAndFlush(justificacion);

        // Get the justificacion
        restJustificacionMockMvc.perform(get("/api/justificacions/{id}", justificacion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(justificacion.getId().intValue()))
            .andExpect(jsonPath("$.desde").value(DEFAULT_DESDE.toString()))
            .andExpect(jsonPath("$.hasta").value(DEFAULT_HASTA.toString()))
            .andExpect(jsonPath("$.motivo").value(DEFAULT_MOTIVO.toString()))
            .andExpect(jsonPath("$.observaciones").value(DEFAULT_OBSERVACIONES));
    }

    @Test
    @Transactional
    public void getNonExistingJustificacion() throws Exception {
        // Get the justificacion
        restJustificacionMockMvc.perform(get("/api/justificacions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJustificacion() throws Exception {
        // Initialize the database
        justificacionRepository.saveAndFlush(justificacion);

        int databaseSizeBeforeUpdate = justificacionRepository.findAll().size();

        // Update the justificacion
        Justificacion updatedJustificacion = justificacionRepository.findById(justificacion.getId()).get();
        // Disconnect from session so that the updates on updatedJustificacion are not directly saved in db
        em.detach(updatedJustificacion);
        updatedJustificacion
            .desde(UPDATED_DESDE)
            .hasta(UPDATED_HASTA)
            .motivo(UPDATED_MOTIVO)
            .observaciones(UPDATED_OBSERVACIONES);

        restJustificacionMockMvc.perform(put("/api/justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedJustificacion)))
            .andExpect(status().isOk());

        // Validate the Justificacion in the database
        List<Justificacion> justificacionList = justificacionRepository.findAll();
        assertThat(justificacionList).hasSize(databaseSizeBeforeUpdate);
        Justificacion testJustificacion = justificacionList.get(justificacionList.size() - 1);
        assertThat(testJustificacion.getDesde()).isEqualTo(UPDATED_DESDE);
        assertThat(testJustificacion.getHasta()).isEqualTo(UPDATED_HASTA);
        assertThat(testJustificacion.getMotivo()).isEqualTo(UPDATED_MOTIVO);
        assertThat(testJustificacion.getObservaciones()).isEqualTo(UPDATED_OBSERVACIONES);
    }

    @Test
    @Transactional
    public void updateNonExistingJustificacion() throws Exception {
        int databaseSizeBeforeUpdate = justificacionRepository.findAll().size();

        // Create the Justificacion

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJustificacionMockMvc.perform(put("/api/justificacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(justificacion)))
            .andExpect(status().isBadRequest());

        // Validate the Justificacion in the database
        List<Justificacion> justificacionList = justificacionRepository.findAll();
        assertThat(justificacionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteJustificacion() throws Exception {
        // Initialize the database
        justificacionRepository.saveAndFlush(justificacion);

        int databaseSizeBeforeDelete = justificacionRepository.findAll().size();

        // Delete the justificacion
        restJustificacionMockMvc.perform(delete("/api/justificacions/{id}", justificacion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Justificacion> justificacionList = justificacionRepository.findAll();
        assertThat(justificacionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Justificacion.class);
        Justificacion justificacion1 = new Justificacion();
        justificacion1.setId(1L);
        Justificacion justificacion2 = new Justificacion();
        justificacion2.setId(justificacion1.getId());
        assertThat(justificacion1).isEqualTo(justificacion2);
        justificacion2.setId(2L);
        assertThat(justificacion1).isNotEqualTo(justificacion2);
        justificacion1.setId(null);
        assertThat(justificacion1).isNotEqualTo(justificacion2);
    }
}
