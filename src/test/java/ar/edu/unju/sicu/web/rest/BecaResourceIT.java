package ar.edu.unju.sicu.web.rest;

import ar.edu.unju.sicu.SicuApp;
import ar.edu.unju.sicu.domain.Beca;
import ar.edu.unju.sicu.domain.Beneficiario;
import ar.edu.unju.sicu.repository.BecaRepository;
import ar.edu.unju.sicu.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static ar.edu.unju.sicu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BecaResource} REST controller.
 */
@SpringBootTest(classes = SicuApp.class)
public class BecaResourceIT {

    private static final LocalDate DEFAULT_FECHA_DESDE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_DESDE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_FECHA_HASTA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_HASTA = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_HABILITACION = "AAAAAAAAAA";
    private static final String UPDATED_HABILITACION = "BBBBBBBBBB";

    @Autowired
    private BecaRepository becaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBecaMockMvc;

    private Beca beca;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BecaResource becaResource = new BecaResource(becaRepository);
        this.restBecaMockMvc = MockMvcBuilders.standaloneSetup(becaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Beca createEntity(EntityManager em) {
        Beca beca = new Beca()
            .fechaDesde(DEFAULT_FECHA_DESDE)
            .fechaHasta(DEFAULT_FECHA_HASTA)
            .habilitacion(DEFAULT_HABILITACION);
        // Add required entity
        Beneficiario beneficiario;
        if (TestUtil.findAll(em, Beneficiario.class).isEmpty()) {
            beneficiario = BeneficiarioResourceIT.createEntity(em);
            em.persist(beneficiario);
            em.flush();
        } else {
            beneficiario = TestUtil.findAll(em, Beneficiario.class).get(0);
        }
        beca.setBeneficiario(beneficiario);
        return beca;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Beca createUpdatedEntity(EntityManager em) {
        Beca beca = new Beca()
            .fechaDesde(UPDATED_FECHA_DESDE)
            .fechaHasta(UPDATED_FECHA_HASTA)
            .habilitacion(UPDATED_HABILITACION);
        // Add required entity
        Beneficiario beneficiario;
        if (TestUtil.findAll(em, Beneficiario.class).isEmpty()) {
            beneficiario = BeneficiarioResourceIT.createUpdatedEntity(em);
            em.persist(beneficiario);
            em.flush();
        } else {
            beneficiario = TestUtil.findAll(em, Beneficiario.class).get(0);
        }
        beca.setBeneficiario(beneficiario);
        return beca;
    }

    @BeforeEach
    public void initTest() {
        beca = createEntity(em);
    }

    @Test
    @Transactional
    public void createBeca() throws Exception {
        int databaseSizeBeforeCreate = becaRepository.findAll().size();

        // Create the Beca
        restBecaMockMvc.perform(post("/api/becas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beca)))
            .andExpect(status().isCreated());

        // Validate the Beca in the database
        List<Beca> becaList = becaRepository.findAll();
        assertThat(becaList).hasSize(databaseSizeBeforeCreate + 1);
        Beca testBeca = becaList.get(becaList.size() - 1);
        assertThat(testBeca.getFechaDesde()).isEqualTo(DEFAULT_FECHA_DESDE);
        assertThat(testBeca.getFechaHasta()).isEqualTo(DEFAULT_FECHA_HASTA);
        assertThat(testBeca.getHabilitacion()).isEqualTo(DEFAULT_HABILITACION);
    }

    @Test
    @Transactional
    public void createBecaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = becaRepository.findAll().size();

        // Create the Beca with an existing ID
        beca.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBecaMockMvc.perform(post("/api/becas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beca)))
            .andExpect(status().isBadRequest());

        // Validate the Beca in the database
        List<Beca> becaList = becaRepository.findAll();
        assertThat(becaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFechaDesdeIsRequired() throws Exception {
        int databaseSizeBeforeTest = becaRepository.findAll().size();
        // set the field null
        beca.setFechaDesde(null);

        // Create the Beca, which fails.

        restBecaMockMvc.perform(post("/api/becas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beca)))
            .andExpect(status().isBadRequest());

        List<Beca> becaList = becaRepository.findAll();
        assertThat(becaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFechaHastaIsRequired() throws Exception {
        int databaseSizeBeforeTest = becaRepository.findAll().size();
        // set the field null
        beca.setFechaHasta(null);

        // Create the Beca, which fails.

        restBecaMockMvc.perform(post("/api/becas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beca)))
            .andExpect(status().isBadRequest());

        List<Beca> becaList = becaRepository.findAll();
        assertThat(becaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHabilitacionIsRequired() throws Exception {
        int databaseSizeBeforeTest = becaRepository.findAll().size();
        // set the field null
        beca.setHabilitacion(null);

        // Create the Beca, which fails.

        restBecaMockMvc.perform(post("/api/becas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beca)))
            .andExpect(status().isBadRequest());

        List<Beca> becaList = becaRepository.findAll();
        assertThat(becaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBecas() throws Exception {
        // Initialize the database
        becaRepository.saveAndFlush(beca);

        // Get all the becaList
        restBecaMockMvc.perform(get("/api/becas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(beca.getId().intValue())))
            .andExpect(jsonPath("$.[*].fechaDesde").value(hasItem(DEFAULT_FECHA_DESDE.toString())))
            .andExpect(jsonPath("$.[*].fechaHasta").value(hasItem(DEFAULT_FECHA_HASTA.toString())))
            .andExpect(jsonPath("$.[*].habilitacion").value(hasItem(DEFAULT_HABILITACION)));
    }
    
    @Test
    @Transactional
    public void getBeca() throws Exception {
        // Initialize the database
        becaRepository.saveAndFlush(beca);

        // Get the beca
        restBecaMockMvc.perform(get("/api/becas/{id}", beca.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(beca.getId().intValue()))
            .andExpect(jsonPath("$.fechaDesde").value(DEFAULT_FECHA_DESDE.toString()))
            .andExpect(jsonPath("$.fechaHasta").value(DEFAULT_FECHA_HASTA.toString()))
            .andExpect(jsonPath("$.habilitacion").value(DEFAULT_HABILITACION));
    }

    @Test
    @Transactional
    public void getNonExistingBeca() throws Exception {
        // Get the beca
        restBecaMockMvc.perform(get("/api/becas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBeca() throws Exception {
        // Initialize the database
        becaRepository.saveAndFlush(beca);

        int databaseSizeBeforeUpdate = becaRepository.findAll().size();

        // Update the beca
        Beca updatedBeca = becaRepository.findById(beca.getId()).get();
        // Disconnect from session so that the updates on updatedBeca are not directly saved in db
        em.detach(updatedBeca);
        updatedBeca
            .fechaDesde(UPDATED_FECHA_DESDE)
            .fechaHasta(UPDATED_FECHA_HASTA)
            .habilitacion(UPDATED_HABILITACION);

        restBecaMockMvc.perform(put("/api/becas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBeca)))
            .andExpect(status().isOk());

        // Validate the Beca in the database
        List<Beca> becaList = becaRepository.findAll();
        assertThat(becaList).hasSize(databaseSizeBeforeUpdate);
        Beca testBeca = becaList.get(becaList.size() - 1);
        assertThat(testBeca.getFechaDesde()).isEqualTo(UPDATED_FECHA_DESDE);
        assertThat(testBeca.getFechaHasta()).isEqualTo(UPDATED_FECHA_HASTA);
        assertThat(testBeca.getHabilitacion()).isEqualTo(UPDATED_HABILITACION);
    }

    @Test
    @Transactional
    public void updateNonExistingBeca() throws Exception {
        int databaseSizeBeforeUpdate = becaRepository.findAll().size();

        // Create the Beca

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBecaMockMvc.perform(put("/api/becas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beca)))
            .andExpect(status().isBadRequest());

        // Validate the Beca in the database
        List<Beca> becaList = becaRepository.findAll();
        assertThat(becaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBeca() throws Exception {
        // Initialize the database
        becaRepository.saveAndFlush(beca);

        int databaseSizeBeforeDelete = becaRepository.findAll().size();

        // Delete the beca
        restBecaMockMvc.perform(delete("/api/becas/{id}", beca.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Beca> becaList = becaRepository.findAll();
        assertThat(becaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Beca.class);
        Beca beca1 = new Beca();
        beca1.setId(1L);
        Beca beca2 = new Beca();
        beca2.setId(beca1.getId());
        assertThat(beca1).isEqualTo(beca2);
        beca2.setId(2L);
        assertThat(beca1).isNotEqualTo(beca2);
        beca1.setId(null);
        assertThat(beca1).isNotEqualTo(beca2);
    }
}
