package ar.edu.unju.sicu.web.rest;

import ar.edu.unju.sicu.SicuApp;
import ar.edu.unju.sicu.domain.Beneficiario;
import ar.edu.unju.sicu.repository.BeneficiarioRepository;
import ar.edu.unju.sicu.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ar.edu.unju.sicu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ar.edu.unju.sicu.domain.enumeration.UnidadAcademica;
/**
 * Integration tests for the {@link BeneficiarioResource} REST controller.
 */
@SpringBootTest(classes = SicuApp.class)
public class BeneficiarioResourceIT {

    private static final String DEFAULT_APELLIDO = "AAAAAAAAAA";
    private static final String UPDATED_APELLIDO = "BBBBBBBBBB";

    private static final String DEFAULT_NOMBRES = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRES = "BBBBBBBBBB";

    private static final String DEFAULT_DNI = "AAAAAAAAAA";
    private static final String UPDATED_DNI = "BBBBBBBBBB";

    private static final String DEFAULT_ID_BIBLIOTECA = "AAAAAAAAAA";
    private static final String UPDATED_ID_BIBLIOTECA = "BBBBBBBBBB";

    private static final UnidadAcademica DEFAULT_UNIDAD_ACADEMICA = UnidadAcademica.FI;
    private static final UnidadAcademica UPDATED_UNIDAD_ACADEMICA = UnidadAcademica.FCE;

    private static final byte[] DEFAULT_FOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_FOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FOTO_CONTENT_TYPE = "image/png";

    @Autowired
    private BeneficiarioRepository beneficiarioRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBeneficiarioMockMvc;

    private Beneficiario beneficiario;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BeneficiarioResource beneficiarioResource = new BeneficiarioResource(beneficiarioRepository);
        this.restBeneficiarioMockMvc = MockMvcBuilders.standaloneSetup(beneficiarioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Beneficiario createEntity(EntityManager em) {
        Beneficiario beneficiario = new Beneficiario()
            .apellido(DEFAULT_APELLIDO)
            .nombres(DEFAULT_NOMBRES)
            .dni(DEFAULT_DNI)
            .idBiblioteca(DEFAULT_ID_BIBLIOTECA)
            .unidadAcademica(DEFAULT_UNIDAD_ACADEMICA)
            .foto(DEFAULT_FOTO)
            .fotoContentType(DEFAULT_FOTO_CONTENT_TYPE);
        return beneficiario;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Beneficiario createUpdatedEntity(EntityManager em) {
        Beneficiario beneficiario = new Beneficiario()
            .apellido(UPDATED_APELLIDO)
            .nombres(UPDATED_NOMBRES)
            .dni(UPDATED_DNI)
            .idBiblioteca(UPDATED_ID_BIBLIOTECA)
            .unidadAcademica(UPDATED_UNIDAD_ACADEMICA)
            .foto(UPDATED_FOTO)
            .fotoContentType(UPDATED_FOTO_CONTENT_TYPE);
        return beneficiario;
    }

    @BeforeEach
    public void initTest() {
        beneficiario = createEntity(em);
    }

    @Test
    @Transactional
    public void createBeneficiario() throws Exception {
        int databaseSizeBeforeCreate = beneficiarioRepository.findAll().size();

        // Create the Beneficiario
        restBeneficiarioMockMvc.perform(post("/api/beneficiarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beneficiario)))
            .andExpect(status().isCreated());

        // Validate the Beneficiario in the database
        List<Beneficiario> beneficiarioList = beneficiarioRepository.findAll();
        assertThat(beneficiarioList).hasSize(databaseSizeBeforeCreate + 1);
        Beneficiario testBeneficiario = beneficiarioList.get(beneficiarioList.size() - 1);
        assertThat(testBeneficiario.getApellido()).isEqualTo(DEFAULT_APELLIDO);
        assertThat(testBeneficiario.getNombres()).isEqualTo(DEFAULT_NOMBRES);
        assertThat(testBeneficiario.getDni()).isEqualTo(DEFAULT_DNI);
        assertThat(testBeneficiario.getIdBiblioteca()).isEqualTo(DEFAULT_ID_BIBLIOTECA);
        assertThat(testBeneficiario.getUnidadAcademica()).isEqualTo(DEFAULT_UNIDAD_ACADEMICA);
        assertThat(testBeneficiario.getFoto()).isEqualTo(DEFAULT_FOTO);
        assertThat(testBeneficiario.getFotoContentType()).isEqualTo(DEFAULT_FOTO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createBeneficiarioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = beneficiarioRepository.findAll().size();

        // Create the Beneficiario with an existing ID
        beneficiario.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBeneficiarioMockMvc.perform(post("/api/beneficiarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beneficiario)))
            .andExpect(status().isBadRequest());

        // Validate the Beneficiario in the database
        List<Beneficiario> beneficiarioList = beneficiarioRepository.findAll();
        assertThat(beneficiarioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkApellidoIsRequired() throws Exception {
        int databaseSizeBeforeTest = beneficiarioRepository.findAll().size();
        // set the field null
        beneficiario.setApellido(null);

        // Create the Beneficiario, which fails.

        restBeneficiarioMockMvc.perform(post("/api/beneficiarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beneficiario)))
            .andExpect(status().isBadRequest());

        List<Beneficiario> beneficiarioList = beneficiarioRepository.findAll();
        assertThat(beneficiarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNombresIsRequired() throws Exception {
        int databaseSizeBeforeTest = beneficiarioRepository.findAll().size();
        // set the field null
        beneficiario.setNombres(null);

        // Create the Beneficiario, which fails.

        restBeneficiarioMockMvc.perform(post("/api/beneficiarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beneficiario)))
            .andExpect(status().isBadRequest());

        List<Beneficiario> beneficiarioList = beneficiarioRepository.findAll();
        assertThat(beneficiarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDniIsRequired() throws Exception {
        int databaseSizeBeforeTest = beneficiarioRepository.findAll().size();
        // set the field null
        beneficiario.setDni(null);

        // Create the Beneficiario, which fails.

        restBeneficiarioMockMvc.perform(post("/api/beneficiarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beneficiario)))
            .andExpect(status().isBadRequest());

        List<Beneficiario> beneficiarioList = beneficiarioRepository.findAll();
        assertThat(beneficiarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUnidadAcademicaIsRequired() throws Exception {
        int databaseSizeBeforeTest = beneficiarioRepository.findAll().size();
        // set the field null
        beneficiario.setUnidadAcademica(null);

        // Create the Beneficiario, which fails.

        restBeneficiarioMockMvc.perform(post("/api/beneficiarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beneficiario)))
            .andExpect(status().isBadRequest());

        List<Beneficiario> beneficiarioList = beneficiarioRepository.findAll();
        assertThat(beneficiarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBeneficiarios() throws Exception {
        // Initialize the database
        beneficiarioRepository.saveAndFlush(beneficiario);

        // Get all the beneficiarioList
        restBeneficiarioMockMvc.perform(get("/api/beneficiarios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(beneficiario.getId().intValue())))
            .andExpect(jsonPath("$.[*].apellido").value(hasItem(DEFAULT_APELLIDO)))
            .andExpect(jsonPath("$.[*].nombres").value(hasItem(DEFAULT_NOMBRES)))
            .andExpect(jsonPath("$.[*].dni").value(hasItem(DEFAULT_DNI)))
            .andExpect(jsonPath("$.[*].idBiblioteca").value(hasItem(DEFAULT_ID_BIBLIOTECA)))
            .andExpect(jsonPath("$.[*].unidadAcademica").value(hasItem(DEFAULT_UNIDAD_ACADEMICA.toString())))
            .andExpect(jsonPath("$.[*].fotoContentType").value(hasItem(DEFAULT_FOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].foto").value(hasItem(Base64Utils.encodeToString(DEFAULT_FOTO))));
    }
    
    @Test
    @Transactional
    public void getBeneficiario() throws Exception {
        // Initialize the database
        beneficiarioRepository.saveAndFlush(beneficiario);

        // Get the beneficiario
        restBeneficiarioMockMvc.perform(get("/api/beneficiarios/{id}", beneficiario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(beneficiario.getId().intValue()))
            .andExpect(jsonPath("$.apellido").value(DEFAULT_APELLIDO))
            .andExpect(jsonPath("$.nombres").value(DEFAULT_NOMBRES))
            .andExpect(jsonPath("$.dni").value(DEFAULT_DNI))
            .andExpect(jsonPath("$.idBiblioteca").value(DEFAULT_ID_BIBLIOTECA))
            .andExpect(jsonPath("$.unidadAcademica").value(DEFAULT_UNIDAD_ACADEMICA.toString()))
            .andExpect(jsonPath("$.fotoContentType").value(DEFAULT_FOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.foto").value(Base64Utils.encodeToString(DEFAULT_FOTO)));
    }

    @Test
    @Transactional
    public void getNonExistingBeneficiario() throws Exception {
        // Get the beneficiario
        restBeneficiarioMockMvc.perform(get("/api/beneficiarios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBeneficiario() throws Exception {
        // Initialize the database
        beneficiarioRepository.saveAndFlush(beneficiario);

        int databaseSizeBeforeUpdate = beneficiarioRepository.findAll().size();

        // Update the beneficiario
        Beneficiario updatedBeneficiario = beneficiarioRepository.findById(beneficiario.getId()).get();
        // Disconnect from session so that the updates on updatedBeneficiario are not directly saved in db
        em.detach(updatedBeneficiario);
        updatedBeneficiario
            .apellido(UPDATED_APELLIDO)
            .nombres(UPDATED_NOMBRES)
            .dni(UPDATED_DNI)
            .idBiblioteca(UPDATED_ID_BIBLIOTECA)
            .unidadAcademica(UPDATED_UNIDAD_ACADEMICA)
            .foto(UPDATED_FOTO)
            .fotoContentType(UPDATED_FOTO_CONTENT_TYPE);

        restBeneficiarioMockMvc.perform(put("/api/beneficiarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBeneficiario)))
            .andExpect(status().isOk());

        // Validate the Beneficiario in the database
        List<Beneficiario> beneficiarioList = beneficiarioRepository.findAll();
        assertThat(beneficiarioList).hasSize(databaseSizeBeforeUpdate);
        Beneficiario testBeneficiario = beneficiarioList.get(beneficiarioList.size() - 1);
        assertThat(testBeneficiario.getApellido()).isEqualTo(UPDATED_APELLIDO);
        assertThat(testBeneficiario.getNombres()).isEqualTo(UPDATED_NOMBRES);
        assertThat(testBeneficiario.getDni()).isEqualTo(UPDATED_DNI);
        assertThat(testBeneficiario.getIdBiblioteca()).isEqualTo(UPDATED_ID_BIBLIOTECA);
        assertThat(testBeneficiario.getUnidadAcademica()).isEqualTo(UPDATED_UNIDAD_ACADEMICA);
        assertThat(testBeneficiario.getFoto()).isEqualTo(UPDATED_FOTO);
        assertThat(testBeneficiario.getFotoContentType()).isEqualTo(UPDATED_FOTO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingBeneficiario() throws Exception {
        int databaseSizeBeforeUpdate = beneficiarioRepository.findAll().size();

        // Create the Beneficiario

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBeneficiarioMockMvc.perform(put("/api/beneficiarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(beneficiario)))
            .andExpect(status().isBadRequest());

        // Validate the Beneficiario in the database
        List<Beneficiario> beneficiarioList = beneficiarioRepository.findAll();
        assertThat(beneficiarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBeneficiario() throws Exception {
        // Initialize the database
        beneficiarioRepository.saveAndFlush(beneficiario);

        int databaseSizeBeforeDelete = beneficiarioRepository.findAll().size();

        // Delete the beneficiario
        restBeneficiarioMockMvc.perform(delete("/api/beneficiarios/{id}", beneficiario.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Beneficiario> beneficiarioList = beneficiarioRepository.findAll();
        assertThat(beneficiarioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Beneficiario.class);
        Beneficiario beneficiario1 = new Beneficiario();
        beneficiario1.setId(1L);
        Beneficiario beneficiario2 = new Beneficiario();
        beneficiario2.setId(beneficiario1.getId());
        assertThat(beneficiario1).isEqualTo(beneficiario2);
        beneficiario2.setId(2L);
        assertThat(beneficiario1).isNotEqualTo(beneficiario2);
        beneficiario1.setId(null);
        assertThat(beneficiario1).isNotEqualTo(beneficiario2);
    }
}
