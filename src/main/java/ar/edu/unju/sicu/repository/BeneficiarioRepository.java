package ar.edu.unju.sicu.repository;
import ar.edu.unju.sicu.domain.Beneficiario;
import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Beneficiario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BeneficiarioRepository extends JpaRepository<Beneficiario, Long> {

    Optional<Beneficiario> findByDniOrIdBiblioteca(String dni, String idBiblioteca);

}
