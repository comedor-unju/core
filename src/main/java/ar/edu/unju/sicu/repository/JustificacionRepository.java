package ar.edu.unju.sicu.repository;
import ar.edu.unju.sicu.domain.Justificacion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Justificacion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JustificacionRepository extends JpaRepository<Justificacion, Long> {

}
