package ar.edu.unju.sicu.repository;
import ar.edu.unju.sicu.domain.Asistencia;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Asistencia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AsistenciaRepository extends JpaRepository<Asistencia, Long> {

    @Query("select asistencia from Asistencia asistencia where asistencia.registradoPor.login = ?#{principal.username}")
    List<Asistencia> findByRegistradoPorIsCurrentUser();

}
