package ar.edu.unju.sicu.repository;
import ar.edu.unju.sicu.domain.Beca;
import ar.edu.unju.sicu.domain.Beneficiario;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Beca entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BecaRepository extends JpaRepository<Beca, Long> {

    List<Beca> findByBeneficiario(Beneficiario beneficiario);
}
