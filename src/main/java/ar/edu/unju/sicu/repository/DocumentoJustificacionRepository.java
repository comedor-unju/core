package ar.edu.unju.sicu.repository;
import ar.edu.unju.sicu.domain.DocumentoJustificacion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DocumentoJustificacion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DocumentoJustificacionRepository extends JpaRepository<DocumentoJustificacion, Long> {

}
