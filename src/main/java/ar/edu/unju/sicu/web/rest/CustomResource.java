/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unju.sicu.web.rest;

import ar.edu.unju.sicu.domain.Asistencia;
import ar.edu.unju.sicu.domain.Beca;
import ar.edu.unju.sicu.domain.Beneficiario;
import ar.edu.unju.sicu.domain.User;
import ar.edu.unju.sicu.repository.AsistenciaRepository;
import ar.edu.unju.sicu.repository.BecaRepository;
import ar.edu.unju.sicu.repository.BeneficiarioRepository;
import ar.edu.unju.sicu.security.SecurityUtils;
import ar.edu.unju.sicu.service.UserService;
import ar.edu.unju.sicu.service.dto.AccessDTO;
import ar.edu.unju.sicu.web.rest.errors.BeneficiarioNotFoundException;
import ar.edu.unju.sicu.web.rest.errors.NoActiveBecaException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wblanck
 */
@RestController
@RequestMapping("/api")
public class CustomResource {

    private final Logger log = LoggerFactory.getLogger(CustomResource.class);

    private final BeneficiarioRepository beneficiarioRepository;

    private final BecaRepository becaRepository;
    
    private final UserService userService;
    
    private final AsistenciaRepository asistenciaRepository;

    public CustomResource(BeneficiarioRepository beneficiarioRepository, BecaRepository becaRepository, UserService userService, AsistenciaRepository asistenciaRepository) {
        this.beneficiarioRepository = beneficiarioRepository;
        this.becaRepository = becaRepository;
        this.userService = userService;
        this.asistenciaRepository = asistenciaRepository;
    }
     /**
     * {@code GET  /beneficiarios/:id} : find beneficiario by idBiblioteca or DNI.
     *
     * @param query the dni or idBiblioteca of the beneficiario to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the beneficiario, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/beneficiario")
    public ResponseEntity<Beneficiario> searchBeneficiarios(@RequestParam String query) {
        log.debug("REST request to get Beneficiario : {}", query);
        Beneficiario beneficiario = beneficiarioRepository.findByDniOrIdBiblioteca(query, query)
                .orElseThrow(BeneficiarioNotFoundException::new);
        
        Beca becaActiva = becaRepository.findByBeneficiario(beneficiario).stream().filter((beca) -> {
            LocalDate now = LocalDate.now();
            return now.isAfter(beca.getFechaDesde()) && now.isBefore(beca.getFechaHasta());
        }).findFirst()
                .orElseThrow(NoActiveBecaException::new);
        
        //RODO Verrificar que no haya entrado previamente ese día

        return ResponseEntity.ok().body(beneficiario);
    }
    
    @PostMapping("/access")
    public ResponseEntity registrarEntrada(@RequestBody AccessDTO accessDTO) {
        Asistencia asistencia = new Asistencia();
        asistencia.setFechaIngreso(ZonedDateTime.now());
        asistencia.setFoto(accessDTO.getFoto());
        
        // TODO remove hardcoding
        asistencia.setFotoContentType("image/jpeg");
        
        //TODO fixme
        String currentLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(RuntimeException::new);
        User user = userService.getUserWithAuthoritiesByLogin(currentLogin).orElseThrow(RuntimeException::new);
        
        Beneficiario beneficiario = beneficiarioRepository.findById(accessDTO.getIdBeneficiario()).orElseThrow(RuntimeException::new);
        
        asistencia.setRegistradoPor(user);
        asistencia.setBeneficiario(beneficiario);

        if(beneficiario.getFoto() == null || beneficiario.getFoto().length == 0) {
            beneficiario.setFoto(accessDTO.getFoto());
            beneficiario.setFotoContentType("image/jpeg");
            beneficiarioRepository.saveAndFlush(beneficiario);
        }
        
        asistenciaRepository.save(asistencia);
        
        return ResponseEntity.ok().build();
    }
    
    
    @PostMapping("/exit")
    public ResponseEntity registrarSalida(Long IdBeneficiario) {
        // TODO
        // buscar asistencia del día y cerrarla.
        return ResponseEntity.ok().build();
    }
}
