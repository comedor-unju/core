package ar.edu.unju.sicu.web.rest;

import ar.edu.unju.sicu.domain.Beneficiario;
import ar.edu.unju.sicu.repository.BeneficiarioRepository;
import ar.edu.unju.sicu.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ar.edu.unju.sicu.domain.Beneficiario}.
 */
@RestController
@RequestMapping("/api")
public class BeneficiarioResource {

    private final Logger log = LoggerFactory.getLogger(BeneficiarioResource.class);

    private static final String ENTITY_NAME = "beneficiario";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BeneficiarioRepository beneficiarioRepository;

    public BeneficiarioResource(BeneficiarioRepository beneficiarioRepository) {
        this.beneficiarioRepository = beneficiarioRepository;
    }

    /**
     * {@code POST  /beneficiarios} : Create a new beneficiario.
     *
     * @param beneficiario the beneficiario to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new beneficiario, or with status {@code 400 (Bad Request)} if the beneficiario has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/beneficiarios")
    public ResponseEntity<Beneficiario> createBeneficiario(@Valid @RequestBody Beneficiario beneficiario) throws URISyntaxException {
        log.debug("REST request to save Beneficiario : {}", beneficiario);
        if (beneficiario.getId() != null) {
            throw new BadRequestAlertException("A new beneficiario cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Beneficiario result = beneficiarioRepository.save(beneficiario);
        return ResponseEntity.created(new URI("/api/beneficiarios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /beneficiarios} : Updates an existing beneficiario.
     *
     * @param beneficiario the beneficiario to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated beneficiario,
     * or with status {@code 400 (Bad Request)} if the beneficiario is not valid,
     * or with status {@code 500 (Internal Server Error)} if the beneficiario couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/beneficiarios")
    public ResponseEntity<Beneficiario> updateBeneficiario(@Valid @RequestBody Beneficiario beneficiario) throws URISyntaxException {
        log.debug("REST request to update Beneficiario : {}", beneficiario);
        if (beneficiario.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Beneficiario result = beneficiarioRepository.save(beneficiario);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, beneficiario.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /beneficiarios} : get all the beneficiarios.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of beneficiarios in body.
     */
    @GetMapping("/beneficiarios")
    public ResponseEntity<List<Beneficiario>> getAllBeneficiarios(Pageable pageable) {
        log.debug("REST request to get a page of Beneficiarios");
        Page<Beneficiario> page = beneficiarioRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /beneficiarios/:id} : get the "id" beneficiario.
     *
     * @param id the id of the beneficiario to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the beneficiario, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/beneficiarios/{id}")
    public ResponseEntity<Beneficiario> getBeneficiario(@PathVariable Long id) {
        log.debug("REST request to get Beneficiario : {}", id);
        Optional<Beneficiario> beneficiario = beneficiarioRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(beneficiario);
    }

    /**
     * {@code DELETE  /beneficiarios/:id} : delete the "id" beneficiario.
     *
     * @param id the id of the beneficiario to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/beneficiarios/{id}")
    public ResponseEntity<Void> deleteBeneficiario(@PathVariable Long id) {
        log.debug("REST request to delete Beneficiario : {}", id);
        beneficiarioRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
