package ar.edu.unju.sicu.web.rest.errors;

import ar.edu.unju.sicu.web.rest.errors.ErrorConstants;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

/**
 *
 * @author wblanck
 */
public class BeneficiarioNotFoundException extends AbstractThrowableProblem {

    public BeneficiarioNotFoundException() {
        super(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "No se encontró ningún beneficiario con los datos requeridos.", Status.NOT_FOUND);
    }

}
