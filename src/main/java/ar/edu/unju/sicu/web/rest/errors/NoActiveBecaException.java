package ar.edu.unju.sicu.web.rest.errors;

public class NoActiveBecaException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public NoActiveBecaException() {
        super(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "No se encontraron becas activas!", "beca", "noActiveBeca");
    }
}
