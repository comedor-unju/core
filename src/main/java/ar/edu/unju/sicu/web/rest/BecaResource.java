package ar.edu.unju.sicu.web.rest;

import ar.edu.unju.sicu.domain.Beca;
import ar.edu.unju.sicu.repository.BecaRepository;
import ar.edu.unju.sicu.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ar.edu.unju.sicu.domain.Beca}.
 */
@RestController
@RequestMapping("/api")
public class BecaResource {

    private final Logger log = LoggerFactory.getLogger(BecaResource.class);

    private static final String ENTITY_NAME = "beca";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BecaRepository becaRepository;

    public BecaResource(BecaRepository becaRepository) {
        this.becaRepository = becaRepository;
    }

    /**
     * {@code POST  /becas} : Create a new beca.
     *
     * @param beca the beca to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new beca, or with status {@code 400 (Bad Request)} if the beca has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/becas")
    public ResponseEntity<Beca> createBeca(@Valid @RequestBody Beca beca) throws URISyntaxException {
        log.debug("REST request to save Beca : {}", beca);
        if (beca.getId() != null) {
            throw new BadRequestAlertException("A new beca cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Beca result = becaRepository.save(beca);
        return ResponseEntity.created(new URI("/api/becas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /becas} : Updates an existing beca.
     *
     * @param beca the beca to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated beca,
     * or with status {@code 400 (Bad Request)} if the beca is not valid,
     * or with status {@code 500 (Internal Server Error)} if the beca couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/becas")
    public ResponseEntity<Beca> updateBeca(@Valid @RequestBody Beca beca) throws URISyntaxException {
        log.debug("REST request to update Beca : {}", beca);
        if (beca.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Beca result = becaRepository.save(beca);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, beca.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /becas} : get all the becas.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of becas in body.
     */
    @GetMapping("/becas")
    public ResponseEntity<List<Beca>> getAllBecas(Pageable pageable) {
        log.debug("REST request to get a page of Becas");
        Page<Beca> page = becaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /becas/:id} : get the "id" beca.
     *
     * @param id the id of the beca to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the beca, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/becas/{id}")
    public ResponseEntity<Beca> getBeca(@PathVariable Long id) {
        log.debug("REST request to get Beca : {}", id);
        Optional<Beca> beca = becaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(beca);
    }

    /**
     * {@code DELETE  /becas/:id} : delete the "id" beca.
     *
     * @param id the id of the beca to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/becas/{id}")
    public ResponseEntity<Void> deleteBeca(@PathVariable Long id) {
        log.debug("REST request to delete Beca : {}", id);
        becaRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
