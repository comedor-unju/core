/**
 * View Models used by Spring MVC REST controllers.
 */
package ar.edu.unju.sicu.web.rest.vm;
