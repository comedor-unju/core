package ar.edu.unju.sicu.web.rest;

import ar.edu.unju.sicu.domain.DocumentoJustificacion;
import ar.edu.unju.sicu.repository.DocumentoJustificacionRepository;
import ar.edu.unju.sicu.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ar.edu.unju.sicu.domain.DocumentoJustificacion}.
 */
@RestController
@RequestMapping("/api")
public class DocumentoJustificacionResource {

    private final Logger log = LoggerFactory.getLogger(DocumentoJustificacionResource.class);

    private static final String ENTITY_NAME = "documentoJustificacion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DocumentoJustificacionRepository documentoJustificacionRepository;

    public DocumentoJustificacionResource(DocumentoJustificacionRepository documentoJustificacionRepository) {
        this.documentoJustificacionRepository = documentoJustificacionRepository;
    }

    /**
     * {@code POST  /documento-justificacions} : Create a new documentoJustificacion.
     *
     * @param documentoJustificacion the documentoJustificacion to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new documentoJustificacion, or with status {@code 400 (Bad Request)} if the documentoJustificacion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/documento-justificacions")
    public ResponseEntity<DocumentoJustificacion> createDocumentoJustificacion(@Valid @RequestBody DocumentoJustificacion documentoJustificacion) throws URISyntaxException {
        log.debug("REST request to save DocumentoJustificacion : {}", documentoJustificacion);
        if (documentoJustificacion.getId() != null) {
            throw new BadRequestAlertException("A new documentoJustificacion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DocumentoJustificacion result = documentoJustificacionRepository.save(documentoJustificacion);
        return ResponseEntity.created(new URI("/api/documento-justificacions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /documento-justificacions} : Updates an existing documentoJustificacion.
     *
     * @param documentoJustificacion the documentoJustificacion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated documentoJustificacion,
     * or with status {@code 400 (Bad Request)} if the documentoJustificacion is not valid,
     * or with status {@code 500 (Internal Server Error)} if the documentoJustificacion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/documento-justificacions")
    public ResponseEntity<DocumentoJustificacion> updateDocumentoJustificacion(@Valid @RequestBody DocumentoJustificacion documentoJustificacion) throws URISyntaxException {
        log.debug("REST request to update DocumentoJustificacion : {}", documentoJustificacion);
        if (documentoJustificacion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DocumentoJustificacion result = documentoJustificacionRepository.save(documentoJustificacion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, documentoJustificacion.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /documento-justificacions} : get all the documentoJustificacions.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of documentoJustificacions in body.
     */
    @GetMapping("/documento-justificacions")
    public ResponseEntity<List<DocumentoJustificacion>> getAllDocumentoJustificacions(Pageable pageable) {
        log.debug("REST request to get a page of DocumentoJustificacions");
        Page<DocumentoJustificacion> page = documentoJustificacionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /documento-justificacions/:id} : get the "id" documentoJustificacion.
     *
     * @param id the id of the documentoJustificacion to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the documentoJustificacion, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/documento-justificacions/{id}")
    public ResponseEntity<DocumentoJustificacion> getDocumentoJustificacion(@PathVariable Long id) {
        log.debug("REST request to get DocumentoJustificacion : {}", id);
        Optional<DocumentoJustificacion> documentoJustificacion = documentoJustificacionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(documentoJustificacion);
    }

    /**
     * {@code DELETE  /documento-justificacions/:id} : delete the "id" documentoJustificacion.
     *
     * @param id the id of the documentoJustificacion to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/documento-justificacions/{id}")
    public ResponseEntity<Void> deleteDocumentoJustificacion(@PathVariable Long id) {
        log.debug("REST request to delete DocumentoJustificacion : {}", id);
        documentoJustificacionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
