package ar.edu.unju.sicu.web.rest;

import ar.edu.unju.sicu.domain.Justificacion;
import ar.edu.unju.sicu.repository.JustificacionRepository;
import ar.edu.unju.sicu.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ar.edu.unju.sicu.domain.Justificacion}.
 */
@RestController
@RequestMapping("/api")
public class JustificacionResource {

    private final Logger log = LoggerFactory.getLogger(JustificacionResource.class);

    private static final String ENTITY_NAME = "justificacion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JustificacionRepository justificacionRepository;

    public JustificacionResource(JustificacionRepository justificacionRepository) {
        this.justificacionRepository = justificacionRepository;
    }

    /**
     * {@code POST  /justificacions} : Create a new justificacion.
     *
     * @param justificacion the justificacion to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new justificacion, or with status {@code 400 (Bad Request)} if the justificacion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/justificacions")
    public ResponseEntity<Justificacion> createJustificacion(@Valid @RequestBody Justificacion justificacion) throws URISyntaxException {
        log.debug("REST request to save Justificacion : {}", justificacion);
        if (justificacion.getId() != null) {
            throw new BadRequestAlertException("A new justificacion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Justificacion result = justificacionRepository.save(justificacion);
        return ResponseEntity.created(new URI("/api/justificacions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /justificacions} : Updates an existing justificacion.
     *
     * @param justificacion the justificacion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated justificacion,
     * or with status {@code 400 (Bad Request)} if the justificacion is not valid,
     * or with status {@code 500 (Internal Server Error)} if the justificacion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/justificacions")
    public ResponseEntity<Justificacion> updateJustificacion(@Valid @RequestBody Justificacion justificacion) throws URISyntaxException {
        log.debug("REST request to update Justificacion : {}", justificacion);
        if (justificacion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Justificacion result = justificacionRepository.save(justificacion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, justificacion.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /justificacions} : get all the justificacions.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of justificacions in body.
     */
    @GetMapping("/justificacions")
    public ResponseEntity<List<Justificacion>> getAllJustificacions(Pageable pageable) {
        log.debug("REST request to get a page of Justificacions");
        Page<Justificacion> page = justificacionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /justificacions/:id} : get the "id" justificacion.
     *
     * @param id the id of the justificacion to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the justificacion, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/justificacions/{id}")
    public ResponseEntity<Justificacion> getJustificacion(@PathVariable Long id) {
        log.debug("REST request to get Justificacion : {}", id);
        Optional<Justificacion> justificacion = justificacionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(justificacion);
    }

    /**
     * {@code DELETE  /justificacions/:id} : delete the "id" justificacion.
     *
     * @param id the id of the justificacion to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/justificacions/{id}")
    public ResponseEntity<Void> deleteJustificacion(@PathVariable Long id) {
        log.debug("REST request to delete Justificacion : {}", id);
        justificacionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
