package ar.edu.unju.sicu.service.dto;

/**
 *
 * @author wblanck
 */
public class AccessDTO {
    
    private Long idBeneficiario;
    
    private byte[] foto;

    public Long getIdBeneficiario() {
        return idBeneficiario;
    }

    public void setIdBeneficiario(Long idBeneficiario) {
        this.idBeneficiario = idBeneficiario;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }
}
