package ar.edu.unju.sicu.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import ar.edu.unju.sicu.domain.enumeration.UnidadAcademica;

/**
 * A Beneficiario.
 */
@Entity
@Table(name = "beneficiario")
public class Beneficiario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "apellido", nullable = false)
    private String apellido;

    @NotNull
    @Column(name = "nombres", nullable = false)
    private String nombres;

    @NotNull
    @Column(name = "dni", nullable = false, unique = true)
    private String dni;

    @Column(name = "id_biblioteca")
    private String idBiblioteca;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "unidad_academica", nullable = false)
    private UnidadAcademica unidadAcademica;

    @Lob
    @Column(name = "foto")
    private byte[] foto;

    @Column(name = "foto_content_type")
    private String fotoContentType;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public String getNombreCompleto() {
        return String.format("%s, %s", apellido, nombres);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApellido() {
        return apellido;
    }

    public Beneficiario apellido(String apellido) {
        this.apellido = apellido;
        return this;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombres() {
        return nombres;
    }

    public Beneficiario nombres(String nombres) {
        this.nombres = nombres;
        return this;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getDni() {
        return dni;
    }

    public Beneficiario dni(String dni) {
        this.dni = dni;
        return this;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getIdBiblioteca() {
        return idBiblioteca;
    }

    public Beneficiario idBiblioteca(String idBiblioteca) {
        this.idBiblioteca = idBiblioteca;
        return this;
    }

    public void setIdBiblioteca(String idBiblioteca) {
        this.idBiblioteca = idBiblioteca;
    }

    public UnidadAcademica getUnidadAcademica() {
        return unidadAcademica;
    }

    public Beneficiario unidadAcademica(UnidadAcademica unidadAcademica) {
        this.unidadAcademica = unidadAcademica;
        return this;
    }

    public void setUnidadAcademica(UnidadAcademica unidadAcademica) {
        this.unidadAcademica = unidadAcademica;
    }

    public byte[] getFoto() {
        return foto;
    }

    public Beneficiario foto(byte[] foto) {
        this.foto = foto;
        return this;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getFotoContentType() {
        return fotoContentType;
    }

    public Beneficiario fotoContentType(String fotoContentType) {
        this.fotoContentType = fotoContentType;
        return this;
    }

    public void setFotoContentType(String fotoContentType) {
        this.fotoContentType = fotoContentType;
    }

    public User getUser() {
        return user;
    }

    public Beneficiario user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Beneficiario)) {
            return false;
        }
        return id != null && id.equals(((Beneficiario) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Beneficiario{" +
            "id=" + getId() +
            ", apellido='" + getApellido() + "'" +
            ", nombres='" + getNombres() + "'" +
            ", dni='" + getDni() + "'" +
            ", idBiblioteca='" + getIdBiblioteca() + "'" +
            ", unidadAcademica='" + getUnidadAcademica() + "'" +
            ", foto='" + getFoto() + "'" +
            ", fotoContentType='" + getFotoContentType() + "'" +
            "}";
    }
}
