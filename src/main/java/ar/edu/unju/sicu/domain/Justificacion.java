package ar.edu.unju.sicu.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

import ar.edu.unju.sicu.domain.enumeration.MotivoInasistencia;

/**
 * A Justificacion.
 */
@Entity
@Table(name = "justificacion")
public class Justificacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "desde", nullable = false)
    private LocalDate desde;

    @NotNull
    @Column(name = "hasta", nullable = false)
    private LocalDate hasta;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "motivo", nullable = false)
    private MotivoInasistencia motivo;

    @Column(name = "observaciones")
    private String observaciones;

    @ManyToOne(optional = false)
    @NotNull
    private Beneficiario beneficiario;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDesde() {
        return desde;
    }

    public Justificacion desde(LocalDate desde) {
        this.desde = desde;
        return this;
    }

    public void setDesde(LocalDate desde) {
        this.desde = desde;
    }

    public LocalDate getHasta() {
        return hasta;
    }

    public Justificacion hasta(LocalDate hasta) {
        this.hasta = hasta;
        return this;
    }

    public void setHasta(LocalDate hasta) {
        this.hasta = hasta;
    }

    public MotivoInasistencia getMotivo() {
        return motivo;
    }

    public Justificacion motivo(MotivoInasistencia motivo) {
        this.motivo = motivo;
        return this;
    }

    public void setMotivo(MotivoInasistencia motivo) {
        this.motivo = motivo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public Justificacion observaciones(String observaciones) {
        this.observaciones = observaciones;
        return this;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Beneficiario getBeneficiario() {
        return beneficiario;
    }

    public Justificacion beneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
        return this;
    }

    public void setBeneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Justificacion)) {
            return false;
        }
        return id != null && id.equals(((Justificacion) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Justificacion{" +
            "id=" + getId() +
            ", desde='" + getDesde() + "'" +
            ", hasta='" + getHasta() + "'" +
            ", motivo='" + getMotivo() + "'" +
            ", observaciones='" + getObservaciones() + "'" +
            "}";
    }
}
