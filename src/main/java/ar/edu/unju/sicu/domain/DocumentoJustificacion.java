package ar.edu.unju.sicu.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A DocumentoJustificacion.
 */
@Entity
@Table(name = "documento_justificacion")
public class DocumentoJustificacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nombre_archivo", nullable = false)
    private String nombreArchivo;

    
    @Lob
    @Column(name = "file_content", nullable = false)
    private byte[] fileContent;

    @Column(name = "file_content_content_type", nullable = false)
    private String fileContentContentType;

    @ManyToOne(optional = false)
    @NotNull
    private Justificacion justificacion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public DocumentoJustificacion nombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
        return this;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public DocumentoJustificacion fileContent(byte[] fileContent) {
        this.fileContent = fileContent;
        return this;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }

    public String getFileContentContentType() {
        return fileContentContentType;
    }

    public DocumentoJustificacion fileContentContentType(String fileContentContentType) {
        this.fileContentContentType = fileContentContentType;
        return this;
    }

    public void setFileContentContentType(String fileContentContentType) {
        this.fileContentContentType = fileContentContentType;
    }

    public Justificacion getJustificacion() {
        return justificacion;
    }

    public DocumentoJustificacion justificacion(Justificacion justificacion) {
        this.justificacion = justificacion;
        return this;
    }

    public void setJustificacion(Justificacion justificacion) {
        this.justificacion = justificacion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DocumentoJustificacion)) {
            return false;
        }
        return id != null && id.equals(((DocumentoJustificacion) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DocumentoJustificacion{" +
            "id=" + getId() +
            ", nombreArchivo='" + getNombreArchivo() + "'" +
            ", fileContent='" + getFileContent() + "'" +
            ", fileContentContentType='" + getFileContentContentType() + "'" +
            "}";
    }
}
