package ar.edu.unju.sicu.domain.enumeration;

/**
 * The UnidadAcademica enumeration.
 */
public enum UnidadAcademica {
    FI, FCE, FHYCS, FCA, EM
}
