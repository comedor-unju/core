package ar.edu.unju.sicu.domain.enumeration;

/**
 * The MotivoInasistencia enumeration.
 */
public enum MotivoInasistencia {
    ENFERMEDAD, SUSPENSION_CLASES, VIAJE, OTROS
}
