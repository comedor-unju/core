import { IUser } from 'app/core/user/user.model';
import { UnidadAcademica } from 'app/shared/model/enumerations/unidad-academica.model';

export interface IBeneficiario {
  id?: number;
  apellido?: string;
  nombres?: string;
  dni?: string;
  idBiblioteca?: string;
  unidadAcademica?: UnidadAcademica;
  fotoContentType?: string;
  foto?: any;
  user?: IUser;
}

export class Beneficiario implements IBeneficiario {
  constructor(
    public id?: number,
    public apellido?: string,
    public nombres?: string,
    public dni?: string,
    public idBiblioteca?: string,
    public unidadAcademica?: UnidadAcademica,
    public fotoContentType?: string,
    public foto?: any,
    public user?: IUser
  ) {}
}
