import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IBeneficiario } from 'app/shared/model/beneficiario.model';

export interface IAsistencia {
  id?: number;
  fechaIngreso?: Moment;
  fechaSalida?: Moment;
  fotoContentType?: string;
  foto?: any;
  registradoPor?: IUser;
  beneficiario?: IBeneficiario;
}

export class Asistencia implements IAsistencia {
  constructor(
    public id?: number,
    public fechaIngreso?: Moment,
    public fechaSalida?: Moment,
    public fotoContentType?: string,
    public foto?: any,
    public registradoPor?: IUser,
    public beneficiario?: IBeneficiario
  ) {}
}
