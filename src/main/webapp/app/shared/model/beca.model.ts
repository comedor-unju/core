import { Moment } from 'moment';
import { IBeneficiario } from 'app/shared/model/beneficiario.model';

export interface IBeca {
  id?: number;
  fechaDesde?: Moment;
  fechaHasta?: Moment;
  habilitacion?: string;
  beneficiario?: IBeneficiario;
}

export class Beca implements IBeca {
  constructor(
    public id?: number,
    public fechaDesde?: Moment,
    public fechaHasta?: Moment,
    public habilitacion?: string,
    public beneficiario?: IBeneficiario
  ) {}
}
