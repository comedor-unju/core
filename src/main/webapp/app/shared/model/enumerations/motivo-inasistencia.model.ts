export const enum MotivoInasistencia {
  ENFERMEDAD = 'ENFERMEDAD',
  SUSPENSION_CLASES = 'SUSPENSION_CLASES',
  VIAJE = 'VIAJE',
  OTROS = 'OTROS'
}
