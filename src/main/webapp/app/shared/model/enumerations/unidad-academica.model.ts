export const enum UnidadAcademica {
  FI = 'FI',
  FCE = 'FCE',
  FHYCS = 'FHYCS',
  FCA = 'FCA',
  EM = 'EM'
}
