import { Moment } from 'moment';
import { IBeneficiario } from 'app/shared/model/beneficiario.model';
import { MotivoInasistencia } from 'app/shared/model/enumerations/motivo-inasistencia.model';

export interface IJustificacion {
  id?: number;
  desde?: Moment;
  hasta?: Moment;
  motivo?: MotivoInasistencia;
  observaciones?: string;
  beneficiario?: IBeneficiario;
}

export class Justificacion implements IJustificacion {
  constructor(
    public id?: number,
    public desde?: Moment,
    public hasta?: Moment,
    public motivo?: MotivoInasistencia,
    public observaciones?: string,
    public beneficiario?: IBeneficiario
  ) {}
}
