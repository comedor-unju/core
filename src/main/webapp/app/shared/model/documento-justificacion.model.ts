import { IJustificacion } from 'app/shared/model/justificacion.model';

export interface IDocumentoJustificacion {
  id?: number;
  nombreArchivo?: string;
  fileContentContentType?: string;
  fileContent?: any;
  justificacion?: IJustificacion;
}

export class DocumentoJustificacion implements IDocumentoJustificacion {
  constructor(
    public id?: number,
    public nombreArchivo?: string,
    public fileContentContentType?: string,
    public fileContent?: any,
    public justificacion?: IJustificacion
  ) {}
}
