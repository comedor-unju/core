import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SicuSharedModule } from 'app/shared/shared.module';
import { BeneficiarioComponent } from './beneficiario.component';
import { BeneficiarioDetailComponent } from './beneficiario-detail.component';
import { BeneficiarioUpdateComponent } from './beneficiario-update.component';
import { BeneficiarioDeletePopupComponent, BeneficiarioDeleteDialogComponent } from './beneficiario-delete-dialog.component';
import { beneficiarioRoute, beneficiarioPopupRoute } from './beneficiario.route';

const ENTITY_STATES = [...beneficiarioRoute, ...beneficiarioPopupRoute];

@NgModule({
  imports: [SicuSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BeneficiarioComponent,
    BeneficiarioDetailComponent,
    BeneficiarioUpdateComponent,
    BeneficiarioDeleteDialogComponent,
    BeneficiarioDeletePopupComponent
  ],
  entryComponents: [BeneficiarioDeleteDialogComponent]
})
export class SicuBeneficiarioModule {}
