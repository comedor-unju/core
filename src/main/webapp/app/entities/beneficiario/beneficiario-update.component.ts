import { Component, OnInit, ElementRef } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IBeneficiario, Beneficiario } from 'app/shared/model/beneficiario.model';
import { BeneficiarioService } from './beneficiario.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-beneficiario-update',
  templateUrl: './beneficiario-update.component.html'
})
export class BeneficiarioUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    apellido: [null, [Validators.required]],
    nombres: [null, [Validators.required]],
    dni: [null, [Validators.required, Validators.pattern('')]],
    idBiblioteca: [],
    unidadAcademica: [null, [Validators.required]],
    foto: [],
    fotoContentType: [],
    user: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected beneficiarioService: BeneficiarioService,
    protected userService: UserService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ beneficiario }) => {
      this.updateForm(beneficiario);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(beneficiario: IBeneficiario) {
    this.editForm.patchValue({
      id: beneficiario.id,
      apellido: beneficiario.apellido,
      nombres: beneficiario.nombres,
      dni: beneficiario.dni,
      idBiblioteca: beneficiario.idBiblioteca,
      unidadAcademica: beneficiario.unidadAcademica,
      foto: beneficiario.foto,
      fotoContentType: beneficiario.fotoContentType,
      user: beneficiario.user
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const beneficiario = this.createFromForm();
    if (beneficiario.id !== undefined) {
      this.subscribeToSaveResponse(this.beneficiarioService.update(beneficiario));
    } else {
      this.subscribeToSaveResponse(this.beneficiarioService.create(beneficiario));
    }
  }

  private createFromForm(): IBeneficiario {
    return {
      ...new Beneficiario(),
      id: this.editForm.get(['id']).value,
      apellido: this.editForm.get(['apellido']).value,
      nombres: this.editForm.get(['nombres']).value,
      dni: this.editForm.get(['dni']).value,
      idBiblioteca: this.editForm.get(['idBiblioteca']).value,
      unidadAcademica: this.editForm.get(['unidadAcademica']).value,
      fotoContentType: this.editForm.get(['fotoContentType']).value,
      foto: this.editForm.get(['foto']).value,
      user: this.editForm.get(['user']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBeneficiario>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
