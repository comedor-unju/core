import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IBeneficiario } from 'app/shared/model/beneficiario.model';

@Component({
  selector: 'jhi-beneficiario-detail',
  templateUrl: './beneficiario-detail.component.html'
})
export class BeneficiarioDetailComponent implements OnInit {
  beneficiario: IBeneficiario;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ beneficiario }) => {
      this.beneficiario = beneficiario;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
