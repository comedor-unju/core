import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Beneficiario } from 'app/shared/model/beneficiario.model';
import { BeneficiarioService } from './beneficiario.service';
import { BeneficiarioComponent } from './beneficiario.component';
import { BeneficiarioDetailComponent } from './beneficiario-detail.component';
import { BeneficiarioUpdateComponent } from './beneficiario-update.component';
import { BeneficiarioDeletePopupComponent } from './beneficiario-delete-dialog.component';
import { IBeneficiario } from 'app/shared/model/beneficiario.model';

@Injectable({ providedIn: 'root' })
export class BeneficiarioResolve implements Resolve<IBeneficiario> {
  constructor(private service: BeneficiarioService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBeneficiario> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Beneficiario>) => response.ok),
        map((beneficiario: HttpResponse<Beneficiario>) => beneficiario.body)
      );
    }
    return of(new Beneficiario());
  }
}

export const beneficiarioRoute: Routes = [
  {
    path: '',
    component: BeneficiarioComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sicuApp.beneficiario.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BeneficiarioDetailComponent,
    resolve: {
      beneficiario: BeneficiarioResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.beneficiario.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BeneficiarioUpdateComponent,
    resolve: {
      beneficiario: BeneficiarioResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.beneficiario.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BeneficiarioUpdateComponent,
    resolve: {
      beneficiario: BeneficiarioResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.beneficiario.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const beneficiarioPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BeneficiarioDeletePopupComponent,
    resolve: {
      beneficiario: BeneficiarioResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.beneficiario.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
