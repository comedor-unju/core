import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBeneficiario } from 'app/shared/model/beneficiario.model';
import { BeneficiarioService } from './beneficiario.service';

@Component({
  selector: 'jhi-beneficiario-delete-dialog',
  templateUrl: './beneficiario-delete-dialog.component.html'
})
export class BeneficiarioDeleteDialogComponent {
  beneficiario: IBeneficiario;

  constructor(
    protected beneficiarioService: BeneficiarioService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.beneficiarioService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'beneficiarioListModification',
        content: 'Deleted an beneficiario'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-beneficiario-delete-popup',
  template: ''
})
export class BeneficiarioDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ beneficiario }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BeneficiarioDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.beneficiario = beneficiario;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/beneficiario', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/beneficiario', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
