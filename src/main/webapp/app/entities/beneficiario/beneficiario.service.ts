import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBeneficiario } from 'app/shared/model/beneficiario.model';

type EntityResponseType = HttpResponse<IBeneficiario>;
type EntityArrayResponseType = HttpResponse<IBeneficiario[]>;

@Injectable({ providedIn: 'root' })
export class BeneficiarioService {
  public resourceUrl = SERVER_API_URL + 'api/beneficiarios';

  constructor(protected http: HttpClient) {}

  create(beneficiario: IBeneficiario): Observable<EntityResponseType> {
    return this.http.post<IBeneficiario>(this.resourceUrl, beneficiario, { observe: 'response' });
  }

  update(beneficiario: IBeneficiario): Observable<EntityResponseType> {
    return this.http.put<IBeneficiario>(this.resourceUrl, beneficiario, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBeneficiario>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBeneficiario[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
