import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IBeca, Beca } from 'app/shared/model/beca.model';
import { BecaService } from './beca.service';
import { IBeneficiario } from 'app/shared/model/beneficiario.model';
import { BeneficiarioService } from 'app/entities/beneficiario/beneficiario.service';

@Component({
  selector: 'jhi-beca-update',
  templateUrl: './beca-update.component.html'
})
export class BecaUpdateComponent implements OnInit {
  isSaving: boolean;

  beneficiarios: IBeneficiario[];
  fechaDesdeDp: any;
  fechaHastaDp: any;

  editForm = this.fb.group({
    id: [],
    fechaDesde: [null, [Validators.required]],
    fechaHasta: [null, [Validators.required]],
    habilitacion: [null, [Validators.required]],
    beneficiario: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected becaService: BecaService,
    protected beneficiarioService: BeneficiarioService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ beca }) => {
      this.updateForm(beca);
    });
    this.beneficiarioService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBeneficiario[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBeneficiario[]>) => response.body)
      )
      .subscribe((res: IBeneficiario[]) => (this.beneficiarios = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(beca: IBeca) {
    this.editForm.patchValue({
      id: beca.id,
      fechaDesde: beca.fechaDesde,
      fechaHasta: beca.fechaHasta,
      habilitacion: beca.habilitacion,
      beneficiario: beca.beneficiario
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const beca = this.createFromForm();
    if (beca.id !== undefined) {
      this.subscribeToSaveResponse(this.becaService.update(beca));
    } else {
      this.subscribeToSaveResponse(this.becaService.create(beca));
    }
  }

  private createFromForm(): IBeca {
    return {
      ...new Beca(),
      id: this.editForm.get(['id']).value,
      fechaDesde: this.editForm.get(['fechaDesde']).value,
      fechaHasta: this.editForm.get(['fechaHasta']).value,
      habilitacion: this.editForm.get(['habilitacion']).value,
      beneficiario: this.editForm.get(['beneficiario']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBeca>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBeneficiarioById(index: number, item: IBeneficiario) {
    return item.id;
  }
}
