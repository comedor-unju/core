import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBeca } from 'app/shared/model/beca.model';
import { BecaService } from './beca.service';

@Component({
  selector: 'jhi-beca-delete-dialog',
  templateUrl: './beca-delete-dialog.component.html'
})
export class BecaDeleteDialogComponent {
  beca: IBeca;

  constructor(protected becaService: BecaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.becaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'becaListModification',
        content: 'Deleted an beca'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-beca-delete-popup',
  template: ''
})
export class BecaDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ beca }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BecaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.beca = beca;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/beca', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/beca', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
