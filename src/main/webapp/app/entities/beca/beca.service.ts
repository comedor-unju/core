import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBeca } from 'app/shared/model/beca.model';

type EntityResponseType = HttpResponse<IBeca>;
type EntityArrayResponseType = HttpResponse<IBeca[]>;

@Injectable({ providedIn: 'root' })
export class BecaService {
  public resourceUrl = SERVER_API_URL + 'api/becas';

  constructor(protected http: HttpClient) {}

  create(beca: IBeca): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(beca);
    return this.http
      .post<IBeca>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(beca: IBeca): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(beca);
    return this.http
      .put<IBeca>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBeca>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBeca[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(beca: IBeca): IBeca {
    const copy: IBeca = Object.assign({}, beca, {
      fechaDesde: beca.fechaDesde != null && beca.fechaDesde.isValid() ? beca.fechaDesde.format(DATE_FORMAT) : null,
      fechaHasta: beca.fechaHasta != null && beca.fechaHasta.isValid() ? beca.fechaHasta.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fechaDesde = res.body.fechaDesde != null ? moment(res.body.fechaDesde) : null;
      res.body.fechaHasta = res.body.fechaHasta != null ? moment(res.body.fechaHasta) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((beca: IBeca) => {
        beca.fechaDesde = beca.fechaDesde != null ? moment(beca.fechaDesde) : null;
        beca.fechaHasta = beca.fechaHasta != null ? moment(beca.fechaHasta) : null;
      });
    }
    return res;
  }
}
