import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Beca } from 'app/shared/model/beca.model';
import { BecaService } from './beca.service';
import { BecaComponent } from './beca.component';
import { BecaDetailComponent } from './beca-detail.component';
import { BecaUpdateComponent } from './beca-update.component';
import { BecaDeletePopupComponent } from './beca-delete-dialog.component';
import { IBeca } from 'app/shared/model/beca.model';

@Injectable({ providedIn: 'root' })
export class BecaResolve implements Resolve<IBeca> {
  constructor(private service: BecaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBeca> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Beca>) => response.ok),
        map((beca: HttpResponse<Beca>) => beca.body)
      );
    }
    return of(new Beca());
  }
}

export const becaRoute: Routes = [
  {
    path: '',
    component: BecaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sicuApp.beca.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BecaDetailComponent,
    resolve: {
      beca: BecaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.beca.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BecaUpdateComponent,
    resolve: {
      beca: BecaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.beca.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BecaUpdateComponent,
    resolve: {
      beca: BecaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.beca.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const becaPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BecaDeletePopupComponent,
    resolve: {
      beca: BecaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.beca.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
