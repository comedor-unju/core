import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SicuSharedModule } from 'app/shared/shared.module';
import { BecaComponent } from './beca.component';
import { BecaDetailComponent } from './beca-detail.component';
import { BecaUpdateComponent } from './beca-update.component';
import { BecaDeletePopupComponent, BecaDeleteDialogComponent } from './beca-delete-dialog.component';
import { becaRoute, becaPopupRoute } from './beca.route';

const ENTITY_STATES = [...becaRoute, ...becaPopupRoute];

@NgModule({
  imports: [SicuSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [BecaComponent, BecaDetailComponent, BecaUpdateComponent, BecaDeleteDialogComponent, BecaDeletePopupComponent],
  entryComponents: [BecaDeleteDialogComponent]
})
export class SicuBecaModule {}
