import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBeca } from 'app/shared/model/beca.model';

@Component({
  selector: 'jhi-beca-detail',
  templateUrl: './beca-detail.component.html'
})
export class BecaDetailComponent implements OnInit {
  beca: IBeca;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ beca }) => {
      this.beca = beca;
    });
  }

  previousState() {
    window.history.back();
  }
}
