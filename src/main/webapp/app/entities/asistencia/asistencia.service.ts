import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAsistencia } from 'app/shared/model/asistencia.model';

type EntityResponseType = HttpResponse<IAsistencia>;
type EntityArrayResponseType = HttpResponse<IAsistencia[]>;

@Injectable({ providedIn: 'root' })
export class AsistenciaService {
  public resourceUrl = SERVER_API_URL + 'api/asistencias';

  constructor(protected http: HttpClient) {}

  create(asistencia: IAsistencia): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(asistencia);
    return this.http
      .post<IAsistencia>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(asistencia: IAsistencia): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(asistencia);
    return this.http
      .put<IAsistencia>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAsistencia>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAsistencia[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(asistencia: IAsistencia): IAsistencia {
    const copy: IAsistencia = Object.assign({}, asistencia, {
      fechaIngreso: asistencia.fechaIngreso != null && asistencia.fechaIngreso.isValid() ? asistencia.fechaIngreso.toJSON() : null,
      fechaSalida: asistencia.fechaSalida != null && asistencia.fechaSalida.isValid() ? asistencia.fechaSalida.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fechaIngreso = res.body.fechaIngreso != null ? moment(res.body.fechaIngreso) : null;
      res.body.fechaSalida = res.body.fechaSalida != null ? moment(res.body.fechaSalida) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((asistencia: IAsistencia) => {
        asistencia.fechaIngreso = asistencia.fechaIngreso != null ? moment(asistencia.fechaIngreso) : null;
        asistencia.fechaSalida = asistencia.fechaSalida != null ? moment(asistencia.fechaSalida) : null;
      });
    }
    return res;
  }
}
