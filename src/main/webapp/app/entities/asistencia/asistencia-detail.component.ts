import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IAsistencia } from 'app/shared/model/asistencia.model';

@Component({
  selector: 'jhi-asistencia-detail',
  templateUrl: './asistencia-detail.component.html'
})
export class AsistenciaDetailComponent implements OnInit {
  asistencia: IAsistencia;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ asistencia }) => {
      this.asistencia = asistencia;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
