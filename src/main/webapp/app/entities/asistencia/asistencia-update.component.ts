import { Component, OnInit, ElementRef } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IAsistencia, Asistencia } from 'app/shared/model/asistencia.model';
import { AsistenciaService } from './asistencia.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IBeneficiario } from 'app/shared/model/beneficiario.model';
import { BeneficiarioService } from 'app/entities/beneficiario/beneficiario.service';

@Component({
  selector: 'jhi-asistencia-update',
  templateUrl: './asistencia-update.component.html'
})
export class AsistenciaUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  beneficiarios: IBeneficiario[];

  editForm = this.fb.group({
    id: [],
    fechaIngreso: [null, [Validators.required]],
    fechaSalida: [],
    foto: [],
    fotoContentType: [],
    registradoPor: [null, Validators.required],
    beneficiario: [null, Validators.required]
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected asistenciaService: AsistenciaService,
    protected userService: UserService,
    protected beneficiarioService: BeneficiarioService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ asistencia }) => {
      this.updateForm(asistencia);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.beneficiarioService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBeneficiario[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBeneficiario[]>) => response.body)
      )
      .subscribe((res: IBeneficiario[]) => (this.beneficiarios = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(asistencia: IAsistencia) {
    this.editForm.patchValue({
      id: asistencia.id,
      fechaIngreso: asistencia.fechaIngreso != null ? asistencia.fechaIngreso.format(DATE_TIME_FORMAT) : null,
      fechaSalida: asistencia.fechaSalida != null ? asistencia.fechaSalida.format(DATE_TIME_FORMAT) : null,
      foto: asistencia.foto,
      fotoContentType: asistencia.fotoContentType,
      registradoPor: asistencia.registradoPor,
      beneficiario: asistencia.beneficiario
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const asistencia = this.createFromForm();
    if (asistencia.id !== undefined) {
      this.subscribeToSaveResponse(this.asistenciaService.update(asistencia));
    } else {
      this.subscribeToSaveResponse(this.asistenciaService.create(asistencia));
    }
  }

  private createFromForm(): IAsistencia {
    return {
      ...new Asistencia(),
      id: this.editForm.get(['id']).value,
      fechaIngreso:
        this.editForm.get(['fechaIngreso']).value != null ? moment(this.editForm.get(['fechaIngreso']).value, DATE_TIME_FORMAT) : undefined,
      fechaSalida:
        this.editForm.get(['fechaSalida']).value != null ? moment(this.editForm.get(['fechaSalida']).value, DATE_TIME_FORMAT) : undefined,
      fotoContentType: this.editForm.get(['fotoContentType']).value,
      foto: this.editForm.get(['foto']).value,
      registradoPor: this.editForm.get(['registradoPor']).value,
      beneficiario: this.editForm.get(['beneficiario']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAsistencia>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackBeneficiarioById(index: number, item: IBeneficiario) {
    return item.id;
  }
}
