import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'beneficiario',
        loadChildren: () => import('./beneficiario/beneficiario.module').then(m => m.SicuBeneficiarioModule)
      },
      {
        path: 'beca',
        loadChildren: () => import('./beca/beca.module').then(m => m.SicuBecaModule)
      },
      {
        path: 'asistencia',
        loadChildren: () => import('./asistencia/asistencia.module').then(m => m.SicuAsistenciaModule)
      },
      {
        path: 'justificacion',
        loadChildren: () => import('./justificacion/justificacion.module').then(m => m.SicuJustificacionModule)
      },
      {
        path: 'documento-justificacion',
        loadChildren: () => import('./documento-justificacion/documento-justificacion.module').then(m => m.SicuDocumentoJustificacionModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class SicuEntityModule {}
