import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SicuSharedModule } from 'app/shared/shared.module';
import { DocumentoJustificacionComponent } from './documento-justificacion.component';
import { DocumentoJustificacionDetailComponent } from './documento-justificacion-detail.component';
import { DocumentoJustificacionUpdateComponent } from './documento-justificacion-update.component';
import {
  DocumentoJustificacionDeletePopupComponent,
  DocumentoJustificacionDeleteDialogComponent
} from './documento-justificacion-delete-dialog.component';
import { documentoJustificacionRoute, documentoJustificacionPopupRoute } from './documento-justificacion.route';

const ENTITY_STATES = [...documentoJustificacionRoute, ...documentoJustificacionPopupRoute];

@NgModule({
  imports: [SicuSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    DocumentoJustificacionComponent,
    DocumentoJustificacionDetailComponent,
    DocumentoJustificacionUpdateComponent,
    DocumentoJustificacionDeleteDialogComponent,
    DocumentoJustificacionDeletePopupComponent
  ],
  entryComponents: [DocumentoJustificacionDeleteDialogComponent]
})
export class SicuDocumentoJustificacionModule {}
