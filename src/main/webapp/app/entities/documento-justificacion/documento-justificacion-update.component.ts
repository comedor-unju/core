import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IDocumentoJustificacion, DocumentoJustificacion } from 'app/shared/model/documento-justificacion.model';
import { DocumentoJustificacionService } from './documento-justificacion.service';
import { IJustificacion } from 'app/shared/model/justificacion.model';
import { JustificacionService } from 'app/entities/justificacion/justificacion.service';

@Component({
  selector: 'jhi-documento-justificacion-update',
  templateUrl: './documento-justificacion-update.component.html'
})
export class DocumentoJustificacionUpdateComponent implements OnInit {
  isSaving: boolean;

  justificacions: IJustificacion[];

  editForm = this.fb.group({
    id: [],
    nombreArchivo: [null, [Validators.required]],
    fileContent: [null, [Validators.required]],
    fileContentContentType: [],
    justificacion: [null, Validators.required]
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected documentoJustificacionService: DocumentoJustificacionService,
    protected justificacionService: JustificacionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ documentoJustificacion }) => {
      this.updateForm(documentoJustificacion);
    });
    this.justificacionService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IJustificacion[]>) => mayBeOk.ok),
        map((response: HttpResponse<IJustificacion[]>) => response.body)
      )
      .subscribe((res: IJustificacion[]) => (this.justificacions = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(documentoJustificacion: IDocumentoJustificacion) {
    this.editForm.patchValue({
      id: documentoJustificacion.id,
      nombreArchivo: documentoJustificacion.nombreArchivo,
      fileContent: documentoJustificacion.fileContent,
      fileContentContentType: documentoJustificacion.fileContentContentType,
      justificacion: documentoJustificacion.justificacion
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const documentoJustificacion = this.createFromForm();
    if (documentoJustificacion.id !== undefined) {
      this.subscribeToSaveResponse(this.documentoJustificacionService.update(documentoJustificacion));
    } else {
      this.subscribeToSaveResponse(this.documentoJustificacionService.create(documentoJustificacion));
    }
  }

  private createFromForm(): IDocumentoJustificacion {
    return {
      ...new DocumentoJustificacion(),
      id: this.editForm.get(['id']).value,
      nombreArchivo: this.editForm.get(['nombreArchivo']).value,
      fileContentContentType: this.editForm.get(['fileContentContentType']).value,
      fileContent: this.editForm.get(['fileContent']).value,
      justificacion: this.editForm.get(['justificacion']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDocumentoJustificacion>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackJustificacionById(index: number, item: IJustificacion) {
    return item.id;
  }
}
