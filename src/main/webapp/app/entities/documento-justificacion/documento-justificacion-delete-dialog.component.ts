import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDocumentoJustificacion } from 'app/shared/model/documento-justificacion.model';
import { DocumentoJustificacionService } from './documento-justificacion.service';

@Component({
  selector: 'jhi-documento-justificacion-delete-dialog',
  templateUrl: './documento-justificacion-delete-dialog.component.html'
})
export class DocumentoJustificacionDeleteDialogComponent {
  documentoJustificacion: IDocumentoJustificacion;

  constructor(
    protected documentoJustificacionService: DocumentoJustificacionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.documentoJustificacionService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'documentoJustificacionListModification',
        content: 'Deleted an documentoJustificacion'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-documento-justificacion-delete-popup',
  template: ''
})
export class DocumentoJustificacionDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ documentoJustificacion }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DocumentoJustificacionDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.documentoJustificacion = documentoJustificacion;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/documento-justificacion', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/documento-justificacion', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
