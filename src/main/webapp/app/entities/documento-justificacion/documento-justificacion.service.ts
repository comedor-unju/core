import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDocumentoJustificacion } from 'app/shared/model/documento-justificacion.model';

type EntityResponseType = HttpResponse<IDocumentoJustificacion>;
type EntityArrayResponseType = HttpResponse<IDocumentoJustificacion[]>;

@Injectable({ providedIn: 'root' })
export class DocumentoJustificacionService {
  public resourceUrl = SERVER_API_URL + 'api/documento-justificacions';

  constructor(protected http: HttpClient) {}

  create(documentoJustificacion: IDocumentoJustificacion): Observable<EntityResponseType> {
    return this.http.post<IDocumentoJustificacion>(this.resourceUrl, documentoJustificacion, { observe: 'response' });
  }

  update(documentoJustificacion: IDocumentoJustificacion): Observable<EntityResponseType> {
    return this.http.put<IDocumentoJustificacion>(this.resourceUrl, documentoJustificacion, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDocumentoJustificacion>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDocumentoJustificacion[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
