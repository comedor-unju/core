import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DocumentoJustificacion } from 'app/shared/model/documento-justificacion.model';
import { DocumentoJustificacionService } from './documento-justificacion.service';
import { DocumentoJustificacionComponent } from './documento-justificacion.component';
import { DocumentoJustificacionDetailComponent } from './documento-justificacion-detail.component';
import { DocumentoJustificacionUpdateComponent } from './documento-justificacion-update.component';
import { DocumentoJustificacionDeletePopupComponent } from './documento-justificacion-delete-dialog.component';
import { IDocumentoJustificacion } from 'app/shared/model/documento-justificacion.model';

@Injectable({ providedIn: 'root' })
export class DocumentoJustificacionResolve implements Resolve<IDocumentoJustificacion> {
  constructor(private service: DocumentoJustificacionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDocumentoJustificacion> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<DocumentoJustificacion>) => response.ok),
        map((documentoJustificacion: HttpResponse<DocumentoJustificacion>) => documentoJustificacion.body)
      );
    }
    return of(new DocumentoJustificacion());
  }
}

export const documentoJustificacionRoute: Routes = [
  {
    path: '',
    component: DocumentoJustificacionComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sicuApp.documentoJustificacion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DocumentoJustificacionDetailComponent,
    resolve: {
      documentoJustificacion: DocumentoJustificacionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.documentoJustificacion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DocumentoJustificacionUpdateComponent,
    resolve: {
      documentoJustificacion: DocumentoJustificacionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.documentoJustificacion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DocumentoJustificacionUpdateComponent,
    resolve: {
      documentoJustificacion: DocumentoJustificacionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.documentoJustificacion.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const documentoJustificacionPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DocumentoJustificacionDeletePopupComponent,
    resolve: {
      documentoJustificacion: DocumentoJustificacionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.documentoJustificacion.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
