import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IDocumentoJustificacion } from 'app/shared/model/documento-justificacion.model';

@Component({
  selector: 'jhi-documento-justificacion-detail',
  templateUrl: './documento-justificacion-detail.component.html'
})
export class DocumentoJustificacionDetailComponent implements OnInit {
  documentoJustificacion: IDocumentoJustificacion;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ documentoJustificacion }) => {
      this.documentoJustificacion = documentoJustificacion;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
