import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Justificacion } from 'app/shared/model/justificacion.model';
import { JustificacionService } from './justificacion.service';
import { JustificacionComponent } from './justificacion.component';
import { JustificacionDetailComponent } from './justificacion-detail.component';
import { JustificacionUpdateComponent } from './justificacion-update.component';
import { JustificacionDeletePopupComponent } from './justificacion-delete-dialog.component';
import { IJustificacion } from 'app/shared/model/justificacion.model';

@Injectable({ providedIn: 'root' })
export class JustificacionResolve implements Resolve<IJustificacion> {
  constructor(private service: JustificacionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IJustificacion> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Justificacion>) => response.ok),
        map((justificacion: HttpResponse<Justificacion>) => justificacion.body)
      );
    }
    return of(new Justificacion());
  }
}

export const justificacionRoute: Routes = [
  {
    path: '',
    component: JustificacionComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sicuApp.justificacion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: JustificacionDetailComponent,
    resolve: {
      justificacion: JustificacionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.justificacion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: JustificacionUpdateComponent,
    resolve: {
      justificacion: JustificacionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.justificacion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: JustificacionUpdateComponent,
    resolve: {
      justificacion: JustificacionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.justificacion.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const justificacionPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: JustificacionDeletePopupComponent,
    resolve: {
      justificacion: JustificacionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sicuApp.justificacion.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
