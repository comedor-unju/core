import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJustificacion } from 'app/shared/model/justificacion.model';
import { JustificacionService } from './justificacion.service';

@Component({
  selector: 'jhi-justificacion-delete-dialog',
  templateUrl: './justificacion-delete-dialog.component.html'
})
export class JustificacionDeleteDialogComponent {
  justificacion: IJustificacion;

  constructor(
    protected justificacionService: JustificacionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.justificacionService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'justificacionListModification',
        content: 'Deleted an justificacion'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-justificacion-delete-popup',
  template: ''
})
export class JustificacionDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ justificacion }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(JustificacionDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.justificacion = justificacion;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/justificacion', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/justificacion', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
