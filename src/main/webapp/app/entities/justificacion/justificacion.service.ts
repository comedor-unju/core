import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IJustificacion } from 'app/shared/model/justificacion.model';

type EntityResponseType = HttpResponse<IJustificacion>;
type EntityArrayResponseType = HttpResponse<IJustificacion[]>;

@Injectable({ providedIn: 'root' })
export class JustificacionService {
  public resourceUrl = SERVER_API_URL + 'api/justificacions';

  constructor(protected http: HttpClient) {}

  create(justificacion: IJustificacion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(justificacion);
    return this.http
      .post<IJustificacion>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(justificacion: IJustificacion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(justificacion);
    return this.http
      .put<IJustificacion>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IJustificacion>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IJustificacion[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(justificacion: IJustificacion): IJustificacion {
    const copy: IJustificacion = Object.assign({}, justificacion, {
      desde: justificacion.desde != null && justificacion.desde.isValid() ? justificacion.desde.format(DATE_FORMAT) : null,
      hasta: justificacion.hasta != null && justificacion.hasta.isValid() ? justificacion.hasta.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.desde = res.body.desde != null ? moment(res.body.desde) : null;
      res.body.hasta = res.body.hasta != null ? moment(res.body.hasta) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((justificacion: IJustificacion) => {
        justificacion.desde = justificacion.desde != null ? moment(justificacion.desde) : null;
        justificacion.hasta = justificacion.hasta != null ? moment(justificacion.hasta) : null;
      });
    }
    return res;
  }
}
