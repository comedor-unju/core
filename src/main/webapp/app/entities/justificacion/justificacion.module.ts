import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SicuSharedModule } from 'app/shared/shared.module';
import { JustificacionComponent } from './justificacion.component';
import { JustificacionDetailComponent } from './justificacion-detail.component';
import { JustificacionUpdateComponent } from './justificacion-update.component';
import { JustificacionDeletePopupComponent, JustificacionDeleteDialogComponent } from './justificacion-delete-dialog.component';
import { justificacionRoute, justificacionPopupRoute } from './justificacion.route';

const ENTITY_STATES = [...justificacionRoute, ...justificacionPopupRoute];

@NgModule({
  imports: [SicuSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    JustificacionComponent,
    JustificacionDetailComponent,
    JustificacionUpdateComponent,
    JustificacionDeleteDialogComponent,
    JustificacionDeletePopupComponent
  ],
  entryComponents: [JustificacionDeleteDialogComponent]
})
export class SicuJustificacionModule {}
