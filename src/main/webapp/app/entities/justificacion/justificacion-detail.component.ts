import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IJustificacion } from 'app/shared/model/justificacion.model';

@Component({
  selector: 'jhi-justificacion-detail',
  templateUrl: './justificacion-detail.component.html'
})
export class JustificacionDetailComponent implements OnInit {
  justificacion: IJustificacion;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ justificacion }) => {
      this.justificacion = justificacion;
    });
  }

  previousState() {
    window.history.back();
  }
}
