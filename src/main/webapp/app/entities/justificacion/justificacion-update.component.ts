import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IJustificacion, Justificacion } from 'app/shared/model/justificacion.model';
import { JustificacionService } from './justificacion.service';
import { IBeneficiario } from 'app/shared/model/beneficiario.model';
import { BeneficiarioService } from 'app/entities/beneficiario/beneficiario.service';

@Component({
  selector: 'jhi-justificacion-update',
  templateUrl: './justificacion-update.component.html'
})
export class JustificacionUpdateComponent implements OnInit {
  isSaving: boolean;

  beneficiarios: IBeneficiario[];
  desdeDp: any;
  hastaDp: any;

  editForm = this.fb.group({
    id: [],
    desde: [null, [Validators.required]],
    hasta: [null, [Validators.required]],
    motivo: [null, [Validators.required]],
    observaciones: [],
    beneficiario: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected justificacionService: JustificacionService,
    protected beneficiarioService: BeneficiarioService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ justificacion }) => {
      this.updateForm(justificacion);
    });
    this.beneficiarioService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBeneficiario[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBeneficiario[]>) => response.body)
      )
      .subscribe((res: IBeneficiario[]) => (this.beneficiarios = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(justificacion: IJustificacion) {
    this.editForm.patchValue({
      id: justificacion.id,
      desde: justificacion.desde,
      hasta: justificacion.hasta,
      motivo: justificacion.motivo,
      observaciones: justificacion.observaciones,
      beneficiario: justificacion.beneficiario
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const justificacion = this.createFromForm();
    if (justificacion.id !== undefined) {
      this.subscribeToSaveResponse(this.justificacionService.update(justificacion));
    } else {
      this.subscribeToSaveResponse(this.justificacionService.create(justificacion));
    }
  }

  private createFromForm(): IJustificacion {
    return {
      ...new Justificacion(),
      id: this.editForm.get(['id']).value,
      desde: this.editForm.get(['desde']).value,
      hasta: this.editForm.get(['hasta']).value,
      motivo: this.editForm.get(['motivo']).value,
      observaciones: this.editForm.get(['observaciones']).value,
      beneficiario: this.editForm.get(['beneficiario']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJustificacion>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBeneficiarioById(index: number, item: IBeneficiario) {
    return item.id;
  }
}
